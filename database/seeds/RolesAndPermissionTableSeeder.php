<?php

use App\Agenwin\Role;
use App\Agenwin\Permission;

use Illuminate\Database\Seeder;

class RolesAndPermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('permissions')->delete();

        Role::create([
					'name'        => 'admin',
					'description' => 'The administrator of the site.'
        ]);

        Role::create([
					'name'        => 'cs',
					'description' => 'The customer support of the site.'
        ]);

        Role::create([
					'name'        => 'member',
					'description' => 'The main user of the app.'
        ]);

        Permission::create([
        	'name' => 'access_system_panel',
        	'description' => 'Access the System Panel'
        ]);


        $role = Role::where('name', 'admin')->first();

        $perm = Permission::first();

        $role->givePermissionTo($perm);
    }
}
