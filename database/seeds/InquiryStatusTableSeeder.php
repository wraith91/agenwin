<?php

use App\Agenwin\InquiryStatus;

use Illuminate\Database\Seeder;

class InquiryStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('inquiry_status')->delete();

        InquiryStatus::create([
					'name'        => 'Pending'
        ]);

        InquiryStatus::create([
					'name'        => 'Solved'
        ]);

        InquiryStatus::create([
					'name'        => 'Rejected'
        ]);

        InquiryStatus::create([
					'name'        => 'Cancelled'
        ]);
    }
}
