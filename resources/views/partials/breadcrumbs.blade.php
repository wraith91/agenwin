<div class="breadcrumb">
	<ul>
		<li><a href="/">Home</a> > </li>
		@foreach($url = explode("/", Request::path()) as $path)
			@if($path != end($url))
				<li><a href=/{{ $path }}>{{ ucfirst($path) }}</a> > </li>
			@else
				<li><strong>{{ urldecode($path) }}</strong>  </li>
			@endif
		@endforeach
	</ul>
</div>