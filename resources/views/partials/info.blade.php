  @if (Session::has('info_message'))
    <section class="flash-region warning">
        <p>{!! Session::get('info_message') !!}</p>
    </section>
  @endif