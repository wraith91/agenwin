@if ($paginator->lastPage() > 1)
<div class="pagination taCenter">
    <a class="pagination-anchor {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}" href="{{ $paginator->url(1) }}">
        &lt;
    </a>        
    @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a class="pagination-anchor {{ ($paginator->currentPage() == $i) ? ' current-page' : '' }}" href="{{ $paginator->url($i) }}">
            {{ $i }}
        </a>
    @endfor
    <a class="pagination-anchor {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}" href="{{ $paginator->url($paginator->currentPage()+1) }}">
        &gt;
    </a>      
</div><!-- pagination -->
@endif