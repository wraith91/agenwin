<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title> @yield('title') </title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="betting">
        <meta name="author" content="jyerro">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <!-- <link rel="stylesheet" type="text/css" href="{{ asset('main/css/vendor/all.css') }}"> -->
        <link rel="stylesheet" type="text/css" href="{{ asset('main/css/admin.css') }}">
        @yield('header')

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="wrapper">
            <header>
                <div class="logo-block">
                    <img src="{{ asset('main/images/logo-agenwin.png') }}" alt="Agenwin">
                </div>
            </header>
            <main>

                @yield('contents')

            </main>
        </div>

        <!-- // <script src="{{ asset('main/js/vendor/all.js') }}"></script> -->
        <script src="{{ asset('main/js/script1.js') }}"></script> <!-- Gem jQuery -->
        <!-- Additional Scripts  -->

        @yield('footer')
    </body>
</html>
