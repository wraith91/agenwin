<div class="aw-footer">
  <div class="bank-footer">
    <div class="container">
      <ul>
        <li>
          <div class="footer-btn">Jam offline bank</div>
        </li>
        <li>
          <div class="bank-captions">
            bank-caption
          </div>
          <img src="{{ asset('main/images/main/bank-2.png') }}" alt="bank logo" />
        </li>
        <li>
          <div class="bank-captions">
            bank-caption
          </div>
          <img src="{{ asset('main/images/main/bank-3.png') }}" alt="bank logo" />
        </li>
        <li>
          <div class="bank-captions">
            bank-caption
          </div>
          <img src="{{ asset('main/images/main/bank-4.png') }}" alt="bank logo" />
        </li>
        <li>
          <div class="bank-captions">
            bank-caption
          </div>
          <img src="{{ asset('main/images/main/bank-5.png') }}" alt="bank logo" />
        </li>
      </ul>
    </div>
  </div>

  <div class="footer-nav">
    <div class="container">
      <ul>
        <li>Syarat &amp ketentuan</li>
        <li>Kebijakan Privasi</li>
        <li>Faqs</li>
        <li>Hubungi Kami</li>
      </ul>
      <div class="footer-copyright">
        Copy right  &#169; 2016 agenwin www.agenwin.com seluruh hak cipta dilindungi
      </div>
    </div>
  </div>
</div>