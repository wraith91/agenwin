<div class="header-nav">
	<ul>
		<a href="/">
			<li class="{{ set_active('/') }}">
				<img src="{{ asset('main/images/main/navbar-1.png') }}" alt="Home">Home
			</li>
		</a>
		<a href="/casino">
			<li class="{{ set_active(['casino', 'casino-*']) }}">
				<img src="{{ asset('main/images/main/navbar-2.png') }}" alt="Casino">Casino
			</li>
		</a>
		<a href="/poker">
			<li class="{{ set_active('poker') }}">
				<img src="{{ asset('main/images/main/navbar-3.png') }}" alt="Poker">Poker
			</li>
		</a>
		<a href="/sabung-ayam">
			<li class="{{ set_active('sabung-ayam') }}">
				<img src="{{ asset('main/images/main/navbar-4.png') }}" alt="Sabung Ayam">Sabung Ayam
			</li>
		</a>
		<a href="/promo">
			<li class="{{ set_active('promo') }}">
				<img src="{{ asset('main/images/main/navbar-6.png') }}" alt="Promo">Promo
			</li>
		</a>
		<a href="/transaction/deposit">
			<li class="{{ set_active('transaction/deposit') }}">
				<img src="{{ asset('main/images/main/navbar-7.png') }}" alt="Deposit">Deposit
			</li>
		</a>
		<a href="/transaction/withdraw">
			<li class="{{ set_active('transaction/withdraw') }}">
				<img src="{{ asset('main/images/main/navbar-8.png') }}" alt="Withdraw">Withdraw
			</li>
		</a>
		<a href="/beritabola">
			<li>
				<img src="{{ asset('main/images/main/navbar-9.png') }}" alt="Berita">Berita
			</li>
		</a>
		<a href="/livescore">
			<li class="{{ set_active('livescore') }}">
				<img src="{{ asset('main/images/main/navbar-8.png') }}" alt="Live Score">Live Score
			</li>
		</a>
		<a href="/tebakbola">
			<li>
				<img src="{{ asset('main/images/main/navbar-10.png') }}" alt="">Tebak Bola
			</li>
		</a>
	</ul>
</div>