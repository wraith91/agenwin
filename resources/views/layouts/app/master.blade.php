<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="theme-color" content="#222222">
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Agenwin</title>
  <link rel="stylesheet" type="text/css" href="{{ asset('main/css/slick.css') }}" />
  <link rel="stylesheet" type="text/css" media="screen and (min-width: 1050px)" href="{{ elixir('main/css/main_desktop.css') }}" />
  <link rel="stylesheet" type="text/css" media="screen and (max-width: 1049px)" href="{{ elixir('main/css/main_mobile.css') }}" />
</head>
<body>

{{--   <div class="popup-parent">
    <a href="http://biangtogel.com/" target="_blank">
      <img src="{{ asset('main/images/popup/popup.png') }}" alt="" class="init-popup">
    </a>
  </div> --}}

  <div class="aw-header">
    <div class="container">
      <div class="aw-logo">
        <img src="{{ asset('main/images/main/logo.png') }}" alt="Site Logo">
        <div class="mobile-handle">
          <div class="mobile-lines line-1"></div>
          <div class="mobile-lines line-2"></div>
          <div class="mobile-lines line-3"></div>
        </div>
      </div>

      @if (Auth::guest())
      <div class="aw-forms">
        {!! Form::open(['route' => 'auth.postLogin', 'class' => 'inline']) !!}
          <div class="form-caption">Member Login</div>
          {!! Form::text('username', null, ['class' => 'aw-inputs', 'placeholder' => 'Username', 'tabindex' => '1']) !!}
          {!! Form::password('password', ['class' => 'aw-inputs', 'placeholder' => 'Password', 'tabindex' => '2']) !!}
          {!! Form::submit('LOGIN', ['class' => 'login-btn']) !!}
        {!! Form::close() !!}
        <a href="/register"><div class="register-btn">Daftar</div></a>
        <a href="#">
          <div class="forget-pass">Forget Password?</div>
        </a>
      </div>
      @else
      <div class="aw-account-module aw-module-active">
        Hello, <span id="username">{{ auth()->user()->username }}</span>
        <span class="drop-arrow"><img src="{{ asset('main/images/main/icon-arrow.png') }}" alt="arrow"></span>
        <ul class="account-module-ddl">
          <a href="{{ "/" . auth()->user()->username }}"><li>Account Details</li></a>
          <a href="#"><li>Change Password</li></a>
          <a href="/auth/logout"><li>Logout</li></a>
        </ul>
      </div>
      @endif
      {{-- @include('layouts.app.contact') --}}
      @include('layouts.app.menu')
    </div>
  </div>

	@yield('contents')

  @include('layouts.app.contact')

  @include('layouts.app.footer')

</body>
	<script type="text/javascript" src="{{ elixir('main/js/all.js') }}"></script>
	@yield('footer')

	<!-- Start of LiveChat (www.livechatinc.com) code -->
	<script type="text/javascript">
		window.__lc = window.__lc || {};
		window.__lc.license = 7544191;
		(function() {
		  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
		  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
		  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
		})();
	</script>
	<!-- End of LiveChat code -->

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-78823595-1', 'auto');
	  ga('send', 'pageview');
	</script>
</html>
