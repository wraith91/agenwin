 <div class="socmed-float">
    <div class="socmed-module">
      <img src="{{ asset('main/images/main/socmed-wa.png') }}" alt="WhatsApp Logo" />
      <span>
        <div class="contact-big">
          +6282279123123
        </div>
        <div class="contact-small">
          WhatsApp
        </div>
      </span>
    </div>
    <div class="socmed-module">
      <img src="{{ asset('main/images/main/socmed-bbm.png') }}" alt="BBM Logo" />
      <span>
        <div class="contact-big">
          2BFB526C
        </div>
        <div class="contact-small">
          BBM
        </div>
      </span>
    </div>
    <div class="socmed-module">
      <img src="{{ asset('main/images/main/socmed-sms.png') }}" alt="SMS Logo" />
      <span>
        <div class="contact-big">
          +6282279123123
        </div>
        <div class="contact-small">
          SMS
        </div>
      </span>
    </div>
    <div class="socmed-module">
      <img src="{{ asset('main/images/main/socmed-yahoo.png') }}" alt="Yahoo Logo" />
      <span>
        <div class="contact-big">
          AGENWIN123
        </div>
        <div class="contact-small">
          Yahoo Messenger
        </div>
      </span>
    </div>
  </div>