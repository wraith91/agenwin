<div class="index-banner-5">
  <div class="container">
    <div class="banner-title-5">
      Sport News
      <div class="news-next">></div>
      <div class="news-prev"><</div>
    </div>
    <div class="news-slider">
      @foreach (array_chunk($articles, 2) as $article)
        <div class="news-slider-item">
          @foreach($article as $detail)
          <div class="news-module">
            <div class="date-container">
              {{ date("d", strtotime($detail['created_at'])) }}
              {{ date("M", strtotime($detail['created_at'])) }}
            </div>
            <img src="{{ asset('http://'. env('IP') .'/' . $detail['image']) }}" alt="News Image"/>
            <div class="news-seperator">
              <div class="news-title">
                <a href="{{ URL::to('/news/' . strtolower($detail['title']))}}">
                  {{ str_limit($detail['title'], 30) }}
                </a>
              </div>
              <div class="news-content">
                {!! str_limit($detail['body'], 120 ) !!}
              </div>
            </div>
          </div>
          @endforeach
        </div>
      @endforeach
    </div>
    <a href="/news">
      <div class="news-button">
        Lihat Berita Lainnya
      </div>
    </a>
  </div>
</div>