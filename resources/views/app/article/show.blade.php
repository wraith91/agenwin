@extends('layouts.app.master')

@section('contents')
    <div class="main-container goal-bg">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="news-detail-container">
        <img src="{{ asset('http://'. env('IP') .'/' . $article->image) }}" alt="{{ $article->title }}" class="news-banner" />
        <div class="news-title">
          {{ $article->title }}
          {{ date("F", mktime(0, 0, 0, $article->created_at->month, 10)) }}
        </div>
        <div class="news-paragraph">
            {!! $article->body !!}
        </div>

{{--         <div class="pagination">
          <div class="previous-arrow">previous</div>
          <div class="next-arrow">next</div>
        </div> --}}
      </div>
    </div>
  </div><!-- main-container -->
@stop
