@extends('layouts.app.master')

@section('contents')
<div class="main-container goal-bg">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="news-container">
        <div class="news-slider-item">
        @foreach ($articles as $article)
          <div class="news-module">
            <a href="{{ URL::to('/news/' . strtolower($article->title))}}">
                <div class="date-container">
                {{ $article->created_at->day }}
                {{ date("M", mktime(0, 0, 0, $article->created_at->month, 10)) }}
                </div>
                <img src="{{ asset('http://'. env('IP') .'/' . $article->image) }}" />
                <div class="news-seperator">
                  <div class="news-title">
                    <strong>
                        {{ str_limit($article->title, 30) }}
                    </strong>
                  </div>
                  <div class="news-content">
                    {!! str_limit($article->body, 130 ) !!}
                  </div>
                </div>
            </a>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </div><!-- main-container -->

    @include('partials.pagination', ['paginator' => $articles])

@stop
