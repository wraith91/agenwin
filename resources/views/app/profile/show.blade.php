@extends('layouts.app.master')

@section('title')
  {!! $user->profile->first_name . " " . $user->profile->last_name !!}
@stop

@section('contents')
<div class="main-container deposit">
    <div class="container">
      @include('partials.breadcrumbs')
      @include ('errors.list')
      @include('partials.flash')
      <div class="login-container">
        {!! Form::model($user->toArray(), ['method' => 'PUT', 'route' => ['member::profile.update', $user->id]]) !!}
          <div class="black-bg">
          <div class="login-title">
            Account Details
          </div>
          <div class="login-modules">
            <span class="login-text">
              Nama Depan*
            </span>
            {!! Form::text('first_name', $user->profile->first_name, ['class' => 'inputs']) !!}
          </div>
          <div class="login-modules">
            <span class="login-text">
              Nama Belakang*
            </span>
            {!! Form::text('last_name', $user->profile->last_name, ['class' => 'inputs']) !!}
          </div>
          <div class="login-modules">
            <span class="login-text">
              Gender*
            </span>
            {!! Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], $user->profile->gender,['class' => 'inputs']) !!}
          </div>
          <div class="login-modules">
            <span class="login-text">
              Nomor HP*
            </span>
            {!! Form::text('mobile', $user->profile->mobile, ['class' => 'inputs']) !!}
          </div>
          <div class="submit-module">
            {!! Form::submit('Update', ['class' => 'submit-btn']) !!}
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div><!-- main-container -->
@stop
