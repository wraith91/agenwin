@extends('layouts.app.master')

@section('contents')
<div class="main-container promo-bg">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="promo-container ">
        <div class="promotion-handle active">
          <img src="{{ asset('main/images/main/promo-banner.png')}}" alt="Promo1" />
          <div class="paragraph">
            <p>
              Bonus rollingan casino sebesar 1.1% akan kami berikan kepada para pemain dalam keadaan menang ataupun kalah pada minggu tersebut.
            </p>
            <p>
              Rollingan/ komisi 1.1% untuk casino (Sbobet/Calibet/IBCbet casino) akan ditambah secara manual ke account anda setiap hari Senin jam 14.00 siang WIB.
            </p>
            <p>
              Hanya berlaku untuk hitungan permainan Casino (Calibet/IBCbet Casino / SBObet Casino ) saja.
            </p>
            <p class="blue">VIP Member (1 credit = 10.000 rupiah)</p>
            <ul>
              <li>Komisi 1.1 % tanpa syarat</li>
            </ul>
            <p class="blue">Normal Member</p>
            <ul>
              <li>Komisi 0,7% ,tanpa syarat.</li>
            </ul>
          </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-1.png')}}" alt="Promo1" />
          <div class="paragraph">
            <p>Promo Mix Parlay untuk master betting mix parlay</p>
            <p>Bonus berlaku untuk semua member</p>
            <p>Promosi ini hanya berlaku untuk taruhan mix parlay</p>
            <p>Jika semua partai yang anda mainkan menang semua dalam 1 paket mix parlay,maka kami akan memberikan saldo lagi untuk kemenangan member</p>
            <p class="callout-info">
                <strong>Contoh :</strong> <br><br>
                mix parlay yang menang full (tidak ada draw dan kalah 1/4)  minimal oddsnya 30.00 <br>
                pasang 100 akan kita berikan bonus tambahan sebesar 100 * 3 ( sesuai odds yang di menangkan 30 - 39 ) = 300 <br>
                100 * 4 ( sesuai odds yang di menangkan 40 - 49 ) = 400 <br>
                100 * 5 ( sesuai odds yang di menangkan 50 - max) = 500 <br>
                Promo bonus ini diberikan setiap hari senin dan member bisa klaim pribadi melalui livechat agenwin.
            </p>
            <p class="callout-warning">
                <strong>PERHATIAN :</strong> <br><br>
                Jika pihak management agenwin mendapatkan bukti atau indikasi kecurangan penyalah-gunaan promo ini (penipuan, manipulasi, bonus hunter, dsb).
                 agenwin  tidak akan mengembalikan dana deposit & membatalkan dana kemenangan member yang bermasalah tersebut.
                 serta agenwin akan menyebarkan nama dan no rekening member yang bermasalah tersebut ke agen lainnya.
                agenwin mengadakan bonus ini sebagai bentuk hiburan & kenyamanan untuk member agenwin.
            </p>
          </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-2-1.png')}}" alt="Promo2" />
          <div class="paragraph">
            <p>
               Bonus Cashback sebesar 5% akan kami berikan kepada para pemain dalam keadaan minus (kalah) pada minggu tersebut.(Dihitung dari total kekalahan pemain setiap periode Senin-Minggu sebelumnya)
            </p>
            <p>
                Bonus akan diberikan setiap hari Senin Sore
            </p>
            <p>
                Minimum Cashback Rp 50.000,- (kalah 1jt) <br><br>
                Tidak ada maximum Cashback (kalah 1 milyar dapet Cashback 50jt)<br><br>
                Minimum Turnover 5x dari total kekalahan. (Jika kalah 5jt maka turnover harus mencapai 25jt).<br><br>
                Jika turnover tidak mencukupi target (dibawah 5x kekalahan ) anda tetap akan mendapatkan Cashback sebesar 3% tanpa batas maksimal (contoh : Kalah 1M akan mendapat Cashback 30jt jika Turnover tidak mencukupi )?<br><br>
                Hanya berlaku untuk hitungan permainan Sportsbook (SBObet / IBCbet ) saja
            </p>
          </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-3.png')}}" alt="Promo3" />
          <div class="paragraph">
            <p>
               30 % BONUS UNTUK SPORTSBOOK (SBOBET, IBCBET)
            </p>
            <p>
                Bonus 30% diberikan untuk deposit awal minimal 1juta.<br><br>
                Bonus maksimal di berikan Rp 3juta
                Maksimal Bet per Match adalah disesuaikan dengan nilai deposit.<br><br>
                Harus capai Turnover 20x dari total nilai deposit. ex : deposit 1.000.000, maka turnover harus capai 20 x (1.000.000+30%) = 26.000.000
                Draw Bet &amp Reject Bet tidak termasuk Turn Over.<br><br>
                Tidak diperkenankan melakukan Bet kiri kanan dalam 1 partai yang sama.<br><br>
                Bonus berlaku untuk seluruh jenis taruhan Sportbooks kecuali Casino.<br><br>
                Bonus tidak berlaku bila ditemukan IP address yang sama dengan member lainnya.<br><br>
                Kami berhak membatalkan Bonus apabila ditemukan kecurangan, No Risk Bet, Abnormal Bett (Manipulasi Bonus).
            </p>
          </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-4.png')}}" alt="Promo4" />
          <div class="paragraph">
          <p>
              Nikmati Pasif income dengan merekomendasikan teman anda bergabung dengan kami.
          </p>
          <p>
              Hanya berlaku untuk sporstbook (sbobet &amp ibcbet) – dapatkan bonus berbentuk commision 0.25% dari total turnover teman anda secara cuma2 (acc teman anda tetap akan mendapatkan commision normal 0.25%, ini hanya bonus tambahan untuk yg mereferensikan).
          </p>
          <p>
              Sewaktu mendaftar anda / teman anda (salah satu) harus menkonfirmasi dengan cs kami anda telah merekomendasikan teman (batas waktu 1x 24jam) Bonus 0.25% commision turnover teman anda akan diberikan ke account anda setiap hari senin. Dihitung periode senin – minggu sebelumnya.
          </p>
          <p>
              Tidak ada batas untuk merekomendasikan teman. Semakin bnyk anda merekomendasikan, semakin bnyk turnover & commision yg anda dapatkan !
          </p>
        </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-5.png')}}" alt="Promo5" />
          <div class="paragraph">
          <p> Bonus akan di berikan setelah menyelesaikan TO 4X dari nilai deposit</p>
          <p>Bonus batal apabila melakukan taruhan Mix parlay,1x2,Correct Score (no risk bet).</p>
          <p class="blue">Minimal Deposit</p>
          <ul>
            <li>Rp200.000</li>
          </ul>
          <p class="blue">Maximal Bonus</p>
          <ul>
            <li>Rp2.000.000</li>
          </ul>
          </div>
        </div>
        <div class="promotion-handle">
          <img src="{{ asset('main/images/main/promo-banners-6.png')}}" alt="Promo6" />
          <div class="paragraph">
            <p>
                Bonus cashback sebesar 3% akan kami berikan kepada pemain dalam keadaan minus (kalah) pada minggu tersebut.
            </p>
            <p>
                (Dihitung dari total kekalahan pemain setiap periode Senin – Minggu sebelumnya)
                Cashback 3% untuk casino akan ditambah oleh staff kami setiap hari Senin.
            </p>
            <ul>
              <li>Minimum Cashback Rp. 30.000,- (kalah 1 juta) &amp Maksimum Cashback Rp. 900.000,- (kalah 30jt)</li>
              <li>Cashback 3% akan diberikan dengan minimal turnover : total kekalahan x 30</li>
            </ul>
            <p class="callout-info">
                <strong>Contoh :</strong> <br><br>
                Jika anda kalah 5000, berarti turnover anda harus 5000 x 30 = 150.000 untuk mendapatkan cashback 3% <br>
                Hanya berlaku untuk permainan Casino (Calibet/sbobet casino/ibcbet casino ) saja
            </p>
          </div>
        </div>
      </div>
    </div>
  </div><!-- main-container -->
@stop

@section('footer')
    <script type="text/javascript" src="{{ asset('main/app/js/promo.js') }}"></script>
@stop
