@extends('layouts.app.master')

@section('contents')

<div class="main-container index">
   <div class="container">
      <div class="index-announcement-slider">
        <div class="index-announcement">
          <span class="news">Berita Terbaru</span> Deposit diatas 50jt silahkan konfirmasi norekening terlebih dahulu
        </div>
        <div class="index-announcement">
          <span class="news">Berita Terbaru</span> Nikmati komisi live casino terbesar sampai dengan 1,1%
        </div>
      </div>
    </div>
    <div class="index-slider-container">
      <div class="index-banner-item">
        <div class="container">
          <div class="slider-text-container">
            <div class="slider-text">
              <span class="large-text">5%</span> <br>
              Cashback
            </div>
            <div class="vertical-line"></div>
            <div class="slider-text">
              <span class="small-text">up to</span> <br>
              <span class="large-text">100</span> jt<br>
              Kekalahan
            </div>
            <a href="/register">
              <div class="banner-btn">
                Daftar
              </div>
            </a>
          </div>
        </div>
        <img src="{{ asset('main/images/index/banner-content-1.png') }}" class="team-img" alt="Banner 1">
      </div>
      <div class="index-banner-item">
        <div class="container">
          <img src="{{ asset('main/images/index/slider-img-2.png') }}" alt="Banner 2" class="slider-img-2">
          <div class="index-banner-text">
            MIX PARLAY
            <div class="white-bold">EXTRA</div>
            <div class="blue-bold">BONUS</div>
            <a href="/register"><div class="blue-btn">DAFTAR</div></a>
          </div>
        </div>
      </div>
      <div class="index-banner-item">
        <div class="container">
          <img src="{{ asset('main/images/index/slider-img-3.png') }}" alt="Banner 3" class="slider-img-3">
          <div class="index-banner-text">
            KOMISI KASINO
            <div class="blue-med">TERBESAR</div>
            <div class="white-xl">1.1 <span class="percentage">%</span></div>
            tanpa syarat
            <a href="/register"><div class="blue-btn">DAFTAR</div></a>
          </div>
        </div>
      </div>
      <div class="index-banner-item">
        <div class="container">
          <img src="{{ asset('main/images/index/slider-img-4.png') }}" alt="Banner 4" class="slider-img-4">
          <div class="index-banner-text">
            PROMO BONUS
            <div class="white-xl">10 <span class="percentage">%</span></div>
            SETIAP DEPOSIT
            <div class="blue-med">SPORTSBOOK</div>
            <a href="/register"><div class="blue-btn">DAFTAR</div></a>
          </div>
        </div>
      </div>
      <div class="index-banner-item">
        <div class="container">
          <img src="{{ asset('main/images/index/slider-img-5.png') }}" alt="Banner 5" class="slider-img-5">
          <div class="index-banner-text left">
            BONUS
            <div class="blue-med">FIRST</div>
            <div class="blue-med">DEPOSIT</div>
            <div class="white-xl">30 <span class="percentage">%</span></div>
            <a href="/register"><div class="blue-btn left">DAFTAR</div></a>
          </div>
        </div>
      </div>
      <div class="index-banner-item">
        <div class="container">
          <img src="{{ asset('main/images/index/slider-img-6.png') }}" alt="Banner 6" class="slider-img-6">
          <div class="index-banner-text left">
            PROMO
            <div class="white-bold">MEMBER</div>
            <div class="blue-bold"><span class="blue-light">GET</span>MEMBER</div>
            <a href="/register"><div class="blue-btn left">DAFTAR</div></a>
          </div>
        </div>
      </div>
    </div>

    <div class="index-banner-2">
      <div class="banner-container">
        <div class="container">
        <img src="{{ asset('main/images/index/banner-content-2.png') }}" alt="Content 2 BG" class="roulette-girl">
          <div class="banner-text">
            Mari <span class="blue">bermain</span> dengan kami
            <div class="banner-text-small">
              Make you more life
            </div>
            <a href="/casino">
              <div class="banner-btn">
                Live casino
              </div>
            </a>
          </div>
          <img src="{{ asset('main/images/index/poker-chip1.png') }}" alt="poker chip" class="chips">
          <img src="{{ asset('main/images/index/dice-1.png') }}" alt="dice" class="dice">
          <img src="{{ asset('main/images/index/dice-1.png') }}" alt="dice" class="dice-2">
        </div>
      </div>
    </div>


    <div class="index-banner-3">
      <div class="banner-container">
        <div class="container">
          <img src="{{ asset('main/images/index/iron-man.png') }}" alt="iron-man" class="iron-man">
          <img src="{{ asset('main/images/index/capt-america.png') }}" alt="capt-america" class="capt-america">
          <img src="{{ asset('main/images/index/girl.png') }}" alt="girl" class="girl">
          <div class="banner-text">
            Mainkan <span class="blue">permainanmu</span> sekarang
            <div class="banner-text-small">
              Much Entertain your brain
            </div>
            <a href="/register">
              <div class="banner-btn">
                Play Now
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="index-banner-4">
      <div class="banner-container">
        <div class="container">
          <img src="{{ asset('main/images/index/man-prop.png') }}" alt="man-prop" class="man-prop">
          <div class="banner-text">
            Lihat <span class="blue">Promo terbaru </span>kami
            <div class="banner-text-small">
              More Gain More Benefit
            </div>
            <a href="/promo">
              <div class="banner-btn">
                View Promo
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  @include('layouts.app.news')
</div><!-- main-container -->

  <div class="langkah-container">
    <div class="container">
      <div class="langkah-module left">
        <span class="bigthree">3</span>
        <span class="module-bold">Langkah Mudah</span>
        Daftar &amp Bermain
        <br>
        Bersama Kami
      </div>
      <div class="langkah-module">
        <a href="transaction/deposit">
          <img src="{{ asset('main/images/index/langkah-3.png') }}" alt="instruction" />
          Play
        </a>
      </div>
      <div class="langkah-module">
        <a href="transaction/deposit">
          <img src="{{ asset('main/images/index/langkah-2.png') }}" alt="instruction" />
          Deposit
        </a>
      </div>
      <div class="langkah-module">
        <a href="register">
          <img src="{{ asset('main/images/index/langkah-1.png') }}" alt="instruction" />
          Daftar
      </a>
      </div>
    </div>
  </div><!-- langkah-container -->
@stop