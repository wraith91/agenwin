@extends('layouts.app.master')

@section('contents')
    <div class="main-container casino">
        <div class="container">
          @include('partials.breadcrumbs')
          <div class="casino-module-container">
            <div class="casino-modules"><img src="{{ asset('main/images/main/sbo-logo.png') }}" alt="sbo logo"></div>
            <div class="casino-modules"><img src="{{ asset('main/images/main/ibc-logo.png') }}" alt="ibc logo"></div>
            <div class="casino-modules"><img src="{{ asset('main/images/main/calibet-logo.png') }}" alt="calibet logo"></div>
          </div>
        </div>
    </div>
@stop