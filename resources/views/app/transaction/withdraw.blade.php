@extends('layouts.app.master')

@section('contents')
<div class="main-container deposit">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="withdraw-container">
        <div class="black-bg">
        <div class="title">
          Withdraw
        </div>
        @include('partials.flash')
    @unless (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()))
      <table>
        <thead>
            <tr>
                <th>Date Deposited</th>
                <th>Requested Amount</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($user->transaction as $transaction)
              @if ($transaction->trxn_status_id != 7 && $transaction->trxn_status_id != 4)
                <tr>
                    <td>{{ $transaction->created_at }}</td>
                    <td>{{ abs($transaction->amount) }}</td>
                    <td>{{ $transaction->status->name }}</td>
                    <td>
                    @if ($transaction->trxn_status_id != 4)
                        {!! Form::open(['method' => 'PATCH', 'url' => ['transaction/withdraw', $transaction->id]]) !!}
                            <div class="field-wrapper-block sm">
                              {!! Form::submit('Cancel Withdraw', ['class' => 'form-button default']) !!}
                            </div>
                        {!! Form::close() !!}
                    @endif
                    </td>
                </tr>
              @endif
            @endforeach
        </tbody>
      </table>
  @endif
  <table>
    <thead>
        <tr>
            <th>Date Deposited</th>
            <th>Deposited Amount</th>
            <th>Date Completed</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($user->transaction as $transaction)
            <tr>
                <td>{{ $transaction->created_at }}</td>
                <td>{{ abs($transaction->amount) }}</td>
                <td>{{ $transaction->updated_at }}</td>
                <td>{{ $transaction->status->name }}</td>
            </tr>
        @endforeach
    </tbody>
  </table>
        <div class="subtitle">
          Mohon diisi dengan benar untuk mempercepat transaksi anda dengan kami
        </div>
          @if (!in_array_r('Pending', $user->transaction->toArray()) && !in_array_r('To Be Approve', $user->transaction->toArray()) && !in_array_r('Processing', $user->transaction->toArray()))
            @if (!$user->gameAccount->count() < 1)
              {!! Form::open(['url' => 'transaction', 'onsubmit' => "return confirm('Do you really want to submit the form?');"]) !!}
              {!! Form::hidden('trxn_type_id', '2') !!}
              <div class="login-modules">
                <span class="login-text">
                  Pilih Permainan*
                </span>
                {!! Form::select('game_id', $type, null, ['class' => 'select-permainan deposit-form-component-stdsize']) !!}
              </div>
              <div class="login-modules">
                <span class="login-text">
                  User ID*
                </span>
                {!! Form::text('account_name', $user->username, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required', 'readonly']) !!}
              </div>
              <div class="login-modules">
                <span class="login-text">
                  Jumlah Withdraw*
                </span>
                {!! Form::text('amount', null, ['class' => 'text-jumlah-deposit deposit-form-component-stdsize', 'required' => 'required']) !!}
              </div>
              <div class="submit-module">
                {!! Form::submit('Submit', ['class' => 'submit-btn']) !!}
              </div>
            {!! Form::close() !!}
           @endif
          @endif
        <div class="paragraph">
          Selain menggunakan Formulir ini, Konfirmasi Withdrawal juga dapat dilakukan dengan cara menghubungi customer service bertugas kami melalui Live Chat atau Yahoo Messenger (depo22bola@yahoo.com) dan juga melalui SMS ke nomor 0821-6182-5555 dan juga dari BBM (Pin 58E9B6B3) dengan format SMS :

          <br><br><br>
          (Contoh Format Withdrawal) <br>
          WD/User ID/Jumlah <br>
          WD/aau123456/100.000 <br>
        </div>
      </div>
      </div>
    </div>
  </div><!-- main-container -->
@stop
