@extends('layouts.app')

@section('contents')
	<div class="subpage-titles fadeIn animated">
	  <div class="inner-content">
	      <h2 class="subpage-title">Transaction</h2>
	  </div>
	</div>

  @include ('errors.list')

  @include('partials.flash')
    <div class="content-region">
        <div class="search-block">
          {!! Form::open(['method' => 'GET', 'url' => 'transaction/history']) !!}
            <div class="field-wrapper-inline sm">
              {!! Form::label('trxn_type_id', 'Transaction Type: ') !!}
              {!! Form::select('trxn_type_id', [ 1 => 'Deposit', 2 => 'Withdraw'], Request::get('trxn_type_id'), ['class' => 'form-select']) !!}
              {!! Form::submit('Submit', ['class' => 'form-button active']) !!}
            </div>
          {!! Form::close() !!}
        </div>
        <table class="display-table">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Amount</th>
                    <th>Game Account</th>
                    <th>Date Completed</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @if (count($transactions) > 0)
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->created_at }}</td>
                            <td>{{ number_format( abs($transaction->amount), 0 , '' , '.' ) }}</td>
                            <td>{{ $transaction->gameType->name }}</td>
                            <td>{{ $transaction->completed_at }}</td>
                            <td>{{ $transaction->status->name }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@stop