@extends('layouts.app.master')

@section('contents')
<div class="main-container deposit">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="deposit-container">
        <div class="black-bg">
        <div class="title">
          Deposit
        </div>
        @include ('errors.list')
        <table>
          <thead>
            <tr>
              <th>System ID</th>
              <th>Date Created</th>
              <th>Date Approved</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
              @foreach ($user->transaction as $transaction)
                  <tr>
                      <td>{{ $transaction->created_at }}</td>
                      <td>{{ $transaction->amount }}</td>
                      <td>{{ $transaction->completed_at }}</td>
                      <td>{{ $transaction->status->name }}</td>
                  </tr>
              @endforeach
          </tbody>
        </table>
        <div class="subtitle">
          Moho diisi dengan benar untuk mempercepat transaksi anda dengan kami.
        </div>
        @if (!in_array_r('Pending', $user->transaction->toArray()))
            {!! Form::model($user->bankAccount, ['url' => 'transaction', 'onsubmit' => "return confirm('Do you really want to submit the form?');"]) !!}
                {!! Form::hidden('trxn_type_id', '1') !!}
                <div class="inputs-container">
                  <div class="inputs-module">
                    <span class="input-title">
                      Pilih Permainan *
                    </span>
                    {!! Form::select('game_id', $game_type, null, ['class' => 'aw-inputs']) !!}
                  </div>
                  <div class="inputs-module">
                    <span class="input-title">
                      Account Name *
                    </span>
                    {!! Form::text('account_name', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required', 'readonly']) !!}
                  </div>
                  <div class="inputs-module">
                    <span class="input-title">
                      Account Number *
                    </span>
                    {!! Form::text('account_no', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required', 'readonly']) !!}
                  </div>
                  <div class="inputs-module">
                    <span class="input-title">
                      Jumlah Deposit *
                    </span>
                    {!! Form::text('amount', null, ['class' => 'text-user-id deposit-form-component-stdsize', 'required' => 'required']) !!}
                  </div>
                  <div class="submit-module">
                    {!! Form::submit('Submit', ['class' => 'submit-btn']) !!}
                  </div>
              {!! Form::close() !!}
          @endif
          <hr>
          <div class="paragraph">
            Selain menggunakan Formulir ini, Konfirmasi Deposit juga dapat dilakukan dengan cara menghubungi customer service bertugas kami melalui Live Chat atau Yahoo Messenger (depo11bola@yahoo.com) dan juga melalui SMS ke nomor 0821-6182-5555 dan juga dari BBM (Pin 58E9B6B3) dengan format SMS :
            <br><br>
            (Contoh Format Deposit) <br>
            DP/User ID/Jumlah <br>
            DP/aau123456/100.000 <br>
            <br><br>
            <span class="grey">
              Ketentuan deposit MANDIRI yang transfer melalui mesin ATM, tidak boleh menggunakan MESIN SETORAN TUNAI & JENIS NON CASH/ NON TUNAI. <br>
              Ketentuan deposit BNI yang transfer melalui mesin ATM, member WAJIB/HARUS menggunakan MESIN ATM BNI tidak boleh menggunakan MESIN SETORAN TUNAI / MESIN ATM Bersama / MESIN ATM bank lain. <br>
              Ketentuan deposit BRI yang transfer melalui mesin ATM, member WAJIB/HARUS menggunakan MESIN ATM BRI tidak boleh menggunakan MESIN ATM Bersama / MESIN ATM bank lain. <br>
            </span>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div><!-- main-container -->
@stop