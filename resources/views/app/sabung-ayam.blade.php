@extends('layouts.app.master')

@section('contents')
<div class="main-container chicken-bg">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="subung-ayam-container">
        <div class="black-bg">
          <img src="{{ asset('main/images/main/ayam.png') }}" alt="Logo">
          <div class="ayam-stamp-container">
            <img src="{{ asset('main/images/main/ayam-stamp.png') }}" alt="">
            Dalam setiap organisasi penting bahwa kita berbagi kepercayaan kepada pelanggan.
            Seperti pelanggan kami yang bijaksana layak untuk tahu bahwa kami bangga dengan produk dan pelayanan kami, memberi kepercayaan merupakan visi dan misi kami.
          </div>
          <div class="ayam-paragraph-container">

            <div class="subtitle">
              Visi Dari Entitas yang relevan
            </div>
            <ul>
              <li>
                S128 berdedikasi dan berkomitmen untuk menyediakan integritas tingkat tertinggi. Layanan dan pengalaman bermain untuk memuaskan pelanggan kami saat ini maupun masa depan pelanggan kami.
              </li>
              <li>
                Federasi Nasional dari Permainan Peternak Unggas, Inc adalah Negara terkemuka atas organisasi permainan peternak unggas yang berdedikasi dan berkomitmen untuk mempromosikan olahraga sebagai sebuah industri dan tradisi yang mulia, yang mana sabung dianggap sebagai hobi nasional dan olahraga yang menggambarkan arti sebenarnya dari kejujuran, integritas dan permainan yang adil.
              </li>
              <li>
                PSSC berusaha untuk mnjadi pemimpin pasar dalam Siaran Langsung Sabung dan penyiaran Filipina bersemnagat untuk memberikan kualitas, dilisensikan dan dapat diandalkan.
              </li>
            </ul>

            <div class="subtitle">
              Visi Dari Entitas yang relevan
            </div>
            <ul>
              <li>
                Untuk mengangkat industri permainan unggas sebagai salah satu penggerak ekonomi kami.
              </li>
              <li>
                Untuk mempromosikan olahraga sebagai sumber mata pencaharian untuk jutaan masyarakat Fillipina.
              </li>
              <li>
                Untuk melindungi hak yang melekat dan semangat dari setiap pecinta sabung dan menikmati olahraga sebagai bagian dari tradisi Negara kita.
              </li>
            </ul>
            <a href="register">
                <div class="submit-btn">
                      daftar sekarang
                </div>
            </a>
          </div>
        </div>
      </div>
    </div>
</div><!-- main-container -->
@stop
