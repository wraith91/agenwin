@extends('layouts.app.master')

@section('contents')
  <div class="main-container casino">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="poker-container">
        <img src="{{ asset('main/images/main/poker-banner.png') }} " alt="">
        <a href="register">
            <div class="submit-btn">
                  daftar sekarang
            </div>
        </a>
      </div>
    </div>
  </div><!-- main-container -->
@stop
