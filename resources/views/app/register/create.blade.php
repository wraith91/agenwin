@extends('layouts.app.master')

@section('contents')
<div class="main-container deposit">
    <div class="container">
      @include('partials.breadcrumbs')
      <div class="register-container">
        <div class="black-bg">
        @include ('errors.list')
        <div class="title">Daftar Member</div>
        {!! Form::open(['route' => 'registration.store']) !!}
        <div class="w50">
          <div class="form-modules">
            <span class="form-text">
              Nama Pengguna*
            </span>
             {!! Form::text('username', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Enter your username']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Email*
            </span>
            {!! Form::text('email', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Enter your valid email']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Password*
            </span>
            {!! Form::password('password', ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Must be min 8 character']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Konfirmasi Password*
            </span>
            {!! Form::password('confirm_password', ['class' => 'login-register-form-item w100', 'required' => 'required']) !!}
          </div>
        </div>
        <div class="w50">
          <div class="form-modules">
            <span class="form-text">
              Nama*
            </span>
            {!! Form::text('first_name', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'Enter your name']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Nomor HP*
            </span>
            {!! Form::text('mobile', null, ['class' => 'login-register-form-item w100', 'placeholder' => 'Enter your mobile number']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Bank*
            </span>
            {!! Form::select('bank_type_id', $bank_name, null, ['class' => 'login-register-form-item w100']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Nama Rekening*
            </span>
            {!! Form::text('account_name', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Enter your bank account name']) !!}
          </div>
          <div class="form-modules">
            <span class="form-text">
              Nomor Rekening*
            </span>
            {!! Form::text('account_no', null, ['class' => 'login-register-form-item w100', 'required' => 'required', 'placeholder' => 'Enter your bank account number']) !!}
          </div>
        </div>
        {!! Form::submit('DAFTAR SEKARANG', ['class' => 'submit-btn']) !!}
        {!! Form::close() !!}
      </div>
      </div>
    </div>
  </div><!-- main-container -->
@stop
