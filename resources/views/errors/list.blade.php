@if ($errors->any())
    <section class="flash-region error">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </section>
@endif

{{-- <div class="warning-container">
    <div class="warning-red">
      <span class="exclaimation">!</span>
        Wrong email or password, please try again or contact us
    </div>
</div> --}}