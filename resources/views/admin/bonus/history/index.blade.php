@extends('layouts.admin.master')

@section('title')
	Transactions: Bonus
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
		'title' => 'Bonus', 
		'secondary_menu' => 
			[
				'Sbobet' => 'admin/bonus/history/1',
				'Ibcbet' => 'admin/bonus/history/2',
				'Calibet' => 'admin/bonus/history/3',
				'Klikpoker' => 'admin/bonus/history/4'
			]
		])
	@include('errors.list')

	@include('partials.flash')
	
	<div class="block block-default">
		  {!! Form::open(['method' => 'GET', 'url' => ['admin/bonus/history', $id]]) !!}
		  	  <div class="field-wrapper-inline sm">
		  	  	{!! Form::select('file', $logs, Input::get('file'), ['class' => 'form-select']) !!}
		  	  </div>
		  	  <div class="field-wrapper-inline sm">
		  	  	{!! Form::submit('Search', ['class' => 'form-button active']) !!}
		  	  </div>
		  {!! Form::close() !!}
	</div>

	<div class="block block-default">
    <div class="block-title"><h4>History Logs:</h4></div>
    <table class="information-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>Username</th>
          <th>Game ID</th>
          <th>Game Type</th>
          <th>Bonus Status</th>
          <th>Bonus Point</th>
        </tr>
      </thead>
      <tbody>
				@if($records)
					@foreach ($records as $index => $record)
		        <tr>
		        	<td>{{ $index+1 }}</td>
		          <td>{!! $record->gameAccount->user->username !!}</td>
		          <td>{!! $record->gameAccount->username !!}</td>
		          <td>{!! $record->gameType->name !!}</td>
		          <td>{!! $record->status->name !!}</td>
		          <td>{!! $record->bonus_point !!}</td>
		        </tr>
					@endforeach
				@endif
      </tbody>
    </table>
  </div>
@stop
