@extends('layouts.admin.master')

@section('title')
	Transactions: Withdraw
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', ['title' => 'Withdraw'])
  {!! Form::open(['method' => 'GET', 'url' => 'admin/transactions/withdraw']) !!}
  	<div class="field-wrapper-inline sm">
  		  {!! Form::label('status', 'Status:') !!}
        {!! Form::select('status', $status, Request::get('status'),['class' => 'form-select']) !!}
  	</div>
    <div class="field-wrapper-inline sm">
        {!! Form::label('date', 'From:') !!}
        {!! Form::input('date', 'from', Request::get('from'),['class' => 'form-select']) !!}
    </div>
    <div class="field-wrapper-inline sm">
        {!! Form::label('date', 'To:') !!}
        {!! Form::input('date', 'to', Request::get('to'),['class' => 'form-select']) !!}
    </div>
  	 <div class="field-wrapper-inline sm">
        {!! Form::submit('Search', ['class' => 'form-button active']) !!} 
     </div>
  {!! Form::close() !!}
   <div class="field-wrapper-inline sm">
      <a href="withdraw/export{{ '?status=' . Input::get('status') . '&from=' . Input::get('from') . '&to=' . Input::get('to') }}" class="form-button default">Export</a>
   </div>
	<div class="block block-default">
    <div class="block-title"><h4>Withdraw Records:</h4></div>
    <table class="information-table">
      <thead>
        <tr>
          <th>Trxn. ID</th>
          <th>System ID</th>
          <th>Username</th>
          <th>Date</th>
          <th>Amount</th>
          <th>Trxn Status</th>
          <th>To Account no.</th>
          <th>To Account name</th>
          <th>Bank Type</th>
          <th>Remarks</th>
        </tr>
      </thead>
      <tbody>
				@foreach ($transactions as $transaction)
	        <tr>
	          <td><a href="{!! 'withdraw/' .$transaction->id !!}">{!! 'W'.$transaction->created_at->format('ymd').$transaction->id!!}</a> </td>
	          <td>{!! 'IN' . str_pad($transaction->user->id, 5, "0", STR_PAD_LEFT) !!}</td>
	          <td>{!! $transaction->user->username !!}</td>
	          <td>{!! $transaction->created_at !!}</td>
	          <td>{!!  number_format( abs($transaction->amount), 0 , '' , '.' ) !!}</td>
	          <td>{!! $transaction->status->name !!}</td>
	          <td>{!! $transaction->bank->account_no !!}</td>
	          <td>{!! $transaction->bank->account_name !!}</td>
	          <td>{!! $transaction->bank->bankName->name !!}</td>
	          <td>{!! $transaction->remarks !!}</td>
	        </tr>
				@endforeach
      </tbody>
    </table>
  </div>
@stop
