@extends('layouts.admin.master')

@section('title')
	Transactions: Withdraw
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
		'title' => 'Withdraw', 
		'secondary_menu' => 
			[
				'Back' => 'admin/transactions/withdraw'
			]
		])
	@include('errors.list')
	@include('partials.flash')
	<div class="block block-default">
		<table class="display-table">
			<tbody>
				<tr>
					<td>Status:</td>
					<td>{!! $transaction->status->name !!}</td>
				</tr>
				<tr>
					<td>Name of Member:</td>
					<td>{!! $transaction->user->profile->first_name . ' ' . $transaction->user->profile->last_name !!}</td>
				</tr>
				<tr>
					<td>Username:</td>
					<td>{!! $transaction->user->username !!}</td>
				</tr>
				<tr>
					<td>System ID:</td>
					<td>{!! 'IN' . str_pad($transaction->user->id, 5, "0", STR_PAD_LEFT) !!}</td>
				</tr>
				<tr>
					<td>Transaction No:</td>
					<td>{!! 'W'.$transaction->created_at->format('ymd').$transaction->id!!}</td>
				</tr>
				<tr>
					<td>Date:</td>
					<td>{!! $transaction->created_at !!}</td>
				</tr>
				<tr>
					<td>Amount:</td>
					<td>{!! number_format( abs($transaction->amount), 0 , '' , '.' ) !!}</td>
				</tr>
				<tr>
					<td>Account Name:</td>
					<td>{!! $transaction->bank->account_name !!}</td>
				</tr>
				<tr>
					<td>Account No:</td>
					<td>{!! $transaction->bank->account_no !!}</td>
				</tr>
				<tr>
					<td>Bank Name:</td>
					<td>{!! $transaction->bank->bankName->name !!}</td>
				</tr>
				<tr>
					<td>Game Type:</td>
					<td>{!! $transaction->gameType->name !!}</td>
				</tr>
				<tr>
					<td>Game Account:</td>
					@if (count($gameAccount) >= 1)
						<td>ID: {!! $gameAccount->username !!}</td>
					@else
						<td>New Member...</td>
					@endif
				</tr>
				<tr>
					<td>Modify By:</td>
					<td>{!! $transaction->modify_by !!}</td>
				</tr>
				<tr>
					<td>Remarks:</td>
					<td>{!! $transaction->remarks !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="block-group">
		<div class="col-6 block">
			<div class="block-title"><h4>Transaction Status:</h4></div>
			{!! Form::open(['method' => 'PATCH', 'url' => ['admin/transactions/withdraw', $transaction->id], 'onsubmit' => "return confirm('Do you really want to submit the form?');"]) !!}
		  	<div class="field-wrapper-block lg">
		      {!! Form::label('trxn_status_id', 'Status:') !!}
		      {!! Form::select('trxn_status_id', $status, null, ['class' => 'form-select']) !!}
		  	</div>
		  	<div class="field-wrapper-block lg">
		      {!! Form::label('remarks', 'Remarks:') !!}
		      {!! Form::textarea('remarks', null, ['class' => 'form-textarea']) !!}
		  	</div>
		  	<div class="field-wrapper-block lg">
		  		{!! Form::submit('Modify', ['class' => 'form-button active']) !!}
		  	</div>
		  {!! Form::close() !!}
		</div>
		<div class="col-6 block">
			<div class="block-title"><h4>Transaction Log:</h4></div>
			@if ($transaction->status->name == 'Approved' && count($game_log) != 1)
				{!! Form::open(['url' => ['admin/transactions/log', $gameAccount->id], 'onsubmit' => "return confirm('Do you really want to submit the form?');"]) !!}
					{!! Form::hidden('transaction_id', $transaction->id) !!}
			  	<div class="field-wrapper-block lg">
			      {!! Form::label('from', 'From:') !!}
			      {!! Form::text('from', null, ['class' => 'form-input']) !!}
			  	</div>
			  	<div class="field-wrapper-block lg">
			      {!! Form::label('to', 'To:') !!}
			      {!! Form::text('to', null, ['class' => 'form-input']) !!}
			  	</div>
			  	<div class="field-wrapper-block lg">
			  		{!! Form::submit('Save', ['class' => 'form-button active']) !!}
			  	</div>
			  {!! Form::close() !!}
		  @endif
		</div>
	</div>
@stop
