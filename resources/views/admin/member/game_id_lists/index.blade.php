@extends('layouts.admin.master')

@section('title')
	Members: Information
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', [
    'title' => 'Member Game ID Lists', 
    'secondary_menu' => 
      [
        'Sbobet' => 'admin/members/game-id-lists/1',
        'Ibcbet' => 'admin/members/game-id-lists/2',
        'Calibet' => 'admin/members/game-id-lists/3',
        'Klikpoker' => 'admin/members/game-id-lists/4'
      ]])
  @include('errors.list')
  <div class="block-group">
      <div class="field-wrapper-inline sm">
        {!! Form::label('search', 'Search:') !!}
        {!! Form::text('search', null, ['class' => 'form-input', 'id' => 'searchInput']) !!}
      </div>
     <div class="field-wrapper-inline sm">
        <a href="export{{ '?id=' . $id = isset($id) ? $id : null }}" class="form-button default">Export</a>
     </div>
  </div>
  <div class="block-group">
    <div class="block">
      <div class="block-title"><h4>Lists:</h4></div>
        <table class="information-table">
          <thead>
            <th>No.</th>
            <th>Username</th>
            <th>Game ID</th>
            <th>Type</th>
          </thead>
          <tbody id="fbody">
            @if(isset($records))
              @foreach ($records as $i => $record)
                <tr>
                  <td>{{ $i+1 }}</td>
                  <td>{{ $record->username }}</td>
                  <td>{{ $record->id }}</td>
                  <td>{{ $record->gameType->name }}</td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
  </div>
@stop

@section('footer')

  <script>
  $("#searchInput").keyup(function () {
    //split the current value of searchInput
    var data = this.value.split(" ");
    //create a jquery object of the rows
    var row = $("#fbody").find("tr");
    if (this.value == "") {
        row.show();
        return;
    }
    //hide all the rows
    row.hide();

    //Recusively filter the jquery object to get results.
    row.filter(function (i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
            if ($t.is(":contains('" + data[d] + "')")) {
                return true;
            }
        }
        return false;
    })
    //show the rows that match.
    .show();
    }).focus(function () {
        this.value = "";
        $(this).unbind('focus');
    });
  </script>

@stop