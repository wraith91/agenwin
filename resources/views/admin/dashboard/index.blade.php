@extends('layouts.admin.master')

@section('title')
	Dashboard
@stop

@section('content')
	@include('layouts.admin.secondary_header_menu', ['title' => 'Dashboard'])

	@include('errors.list')

	<div class="block-group">
		<div class="block dash col-3">
			<div class="dash-title">
				<h4>Users</h4>
			</div>
			<div class="dash-content">
				<h1>{{ $total_members }}</h1>
				<span>Member Registered</span>
			</div>
		</div>
		<div class="block dash col-3">
			<div class="dash-title">
				<h4>Users</h4>
			</div>
			<div class="dash-content">
				<h1>{{ $new_members }}</h1>
				<span>New User This Month</span>
			</div>
		</div>
		<div class="block dash col-3">
			<div class="dash-title">
				<h4>Users</h4>
			</div>
			<div class="dash-content">
				<h1>{{ $active_members }}</h1>
				<span>Active User</span>
			</div>
		</div>
		<div class="block dash col-3">
			<div class="dash-title">
				<h4>Inquiries</h4>
			</div>
			<div class="dash-content">
				<h1>{{ $inquiry }}</h1>
				<span>Pending</span>
			</div>
		</div>
	</div>
@stop