<?php

namespace App\Providers;

Use App\Repositories\Eloquent\User\DbUserRepository;
use App\Repositories\Eloquent\Article\DbArticleRepository;
use App\Repositories\Eloquent\GameType\DbGameTypeRepository;
use App\Repositories\Eloquent\Transaction\DbTransactionRepository;
use App\Repositories\Eloquent\UserGameProfile\DbUserGameProfileRepository;
use App\Repositories\Eloquent\TransactionStatus\DbTransactionStatusRepository;
use App\Repositories\Eloquent\GameTransactionLog\DbGameTransactionLogRepository;

Use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\ArticleRepositoryInterface;
use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;
use App\Repositories\Contracts\TransactionStatusRepositoryInterface;
use App\Repositories\Contracts\GameTransactionLogRepositoryInterface;


use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            UserRepositoryInterface::class,
            DbUserRepository::class
        );
        $this->app->bind(
            TransactionRepositoryInterface::class,
            DbTransactionRepository::class
        );
        $this->app->bind(
            TransactionStatusRepositoryInterface::class,
            DbTransactionStatusRepository::class
        );
        $this->app->bind(
            UserGameProfileRepositoryInterface::class,
            DbUserGameProfileRepository::class
        );
        $this->app->bind(
            GameTypeRepositoryInterface::class,
            DbGameTypeRepository::class
        );
        $this->app->bind(
            GameTransactionLogRepositoryInterface::class,
            DbGameTransactionLogRepository::class
        );
        $this->app->bind(
            ArticleRepositoryInterface::class,
            DbArticleRepository::class
        );
    }
}