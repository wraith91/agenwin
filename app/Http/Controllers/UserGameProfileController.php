<?php

namespace App\Http\Controllers;

use App\Agenwin\User;
use App\Agenwin\UserGameProfile;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserGameProfileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $gameAccount = new UserGameProfile($request->except('_token'));

        if ($user->gameAccount()->save($gameAccount))
        {
            session()->flash('flash_message', 'Game account has been successfully stored!');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = User::with('gameAccount.gameType')->find($id);
        return view('app.game.show', compact('details'));
    }
}
