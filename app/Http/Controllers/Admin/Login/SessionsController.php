<?php

namespace App\Http\Controllers\Admin\Login;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Support\Facades\Input;

class SessionsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginFormRequest $request)
    {
        $input = $request->except('_token', 'remember');

        $remember = (Input::has('remember')) ? true : false;

        if (Auth::attempt($input, $remember))
        {
            return redirect()->intended('/');
        }

        return redirect()->route('auth/login')->withErrors('These credentials do not match our records.');
    }
}
