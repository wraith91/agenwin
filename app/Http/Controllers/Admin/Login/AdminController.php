<?php

namespace App\Http\Controllers\Admin\Login;

use Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.session.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $remember = (isset($request->remember_me)) ? true : false;

        if (Auth::attempt($input, $remember))
        {
            if (Auth::user()->hasRole('member'))
            {
                Auth::logout();
                return redirect()->route('admin.auth.login')->withErrors('You do not have the privilage to access this area!');
            }
            return redirect('admin/dashboard');
        }

        return redirect()->route('admin.auth.login')->withErrors('These credentials do not match our records.');
    }
}
