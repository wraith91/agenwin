<?php

namespace App\Http\Controllers\Admin;

use App\Agenwin\Role;
use App\Repositories\Contracts\UserRepositoryInterface;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\SystemUserRegistrationRequest;

class AuthorizeUserController extends Controller
{
    /**
     * @var App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $user;

    /**
     * @var App\Agenwin\Role
     */
    protected $role;

    public function __construct(UserRepositoryInterface $user, Role $role)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->whereHas('roles', function ($query) {
                                $query->whereIn('name', ['admin', 'cs']);
                            })->get();

        return view('admin.system.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->role->select(DB::raw("CONCAT(name, ' - ', description) AS label, id"))->lists('label', 'id');
        return view('admin.system.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SystemUserRegistrationRequest $request)
    {
        $role = $this->role->find($request->role);
        $user = $this->user->createAuthorizeUser($request->except('role', '_token'));

        if ($user->assignRole($role->name))
        {
            session()->flash('flash_message', 'New ' . strtoupper($role->name) . ' account sucessfully created!');
        }

        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->user->with('profile')->find($id);
        $roles = $this->role->select(DB::raw("CONCAT(name, ' - ', description) AS label, id"))->lists('label', 'id');
        return view('admin.system.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_method', '_token', 'role');

        if ($this->user->update($input, $id) && $this->user->updateProfileById($input, $id))
        {
            session()->flash('flash_message', 'Account successfully updated!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($user = $this->user->delete($id))
        {
            session()->flash('flash_message', $user->username . ' has been sucessfully deleted!');
        }

        return redirect()->back();
    }
}
