<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;
use App\Repositories\Contracts\TransactionStatusRepositoryInterface;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DepositController extends Controller
{
    /**
     * @var App\Repositories\Eloquent\Transaction\DbTransactionRepository
     */
    private $transaction;

    /**
     * @var App\Repositories\Eloquent\Transaction\DbTransactionStatusRepository
     */
    private $transaction_status;

    /**
     * @var App\Repositories\Eloquent\UserGameProfile\DbUserGameProfileRepository;
     */
    private $user_game_profile;

    /**
     * @var App\Repositories\Eloquent\GameType\DbGameTypeRepository;
     */
    private $game_type;

    public function __construct(GameTypeRepositoryInterface $game_type,
                                TransactionRepositoryInterface $transaction,
                                UserGameProfileRepositoryInterface $user_game_profile,
                                TransactionStatusRepositoryInterface $transaction_status)
    {
        $this->transaction = $transaction;
        $this->transaction_status = $transaction_status;
        $this->user_game_profile = $user_game_profile;
        $this->game_type = $game_type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = $this->transaction_status->getStatusWhereNotIn(['5']);
        $transactions = $this->getTransaction();
        return view('admin.deposit.index', compact('transactions', 'status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = $this->transaction->with('status', 'user.profile', 'bank.bankName', 'gameType')->find($id);

        try {
            $gameAccount = $this->user_game_profile->findFirstWhere(['user_id' => $transaction->user_id, 'game_id' => $transaction->game_id]);
        }
        catch (ModelNotFoundException $e) {
            $gameAccount = [];
        }

        $status = $this->transaction_status->getStatusWhereIn(['3', '4']);
        $game = $this->game_type->lists('name');

        return view('admin.deposit.show', compact('transaction', 'status', 'game', 'gameAccount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->trxn_status_id == 4)
        {
            $this->validate($request, ['remarks' => 'required']);
        }

        $request->merge(['modify_by' => auth()->user()->username, 'completed_at' => date('Y-m-d')]);

        if ($this->transaction->update($request->except('_method', '_token'), $id))
        {
            session()->flash('flash_message', 'Transaction has been updated');
        }

        return redirect()->back();
    }

    public function export()
    {
       return Excel::create('Deposit Transactions', function($excel) {

            $excel->setTitle('Deposit Export Sheet');

            $excel->sheet('Sheet 1', function($sheet) {

                $sheet->setFontFamily('Comic Sans MS');
                $transactions = $this->getTransaction();
                $sheet->loadView('admin.deposit.xls')->with('transactions', $transactions);

            });

        })->export('xls');
    }

    public function getTransaction()
    {
        $records = $this->transaction->getDepositTransactions();

        switch (Input::get('status'))
        {
            case 1:
                $transactions = $records;
                break;
            case 2:
                $transactions = $records->pending();
                break;
            case 3:
                $transactions = $records->approved();
                break;
            case 4:
                $transactions = $records->rejected();
                break;
            case 5:
                $transactions = $records->tobeapproved();
                break;
            case 6:
                $transactions = $records->processing();
                break;
            case 7:
                $transactions = $records->cancelled();
                break;
            default:
                $transactions = $records->pending();
                break;
        }

        if (Input::get('from') != null && Input::get('to') != null)
        {
            return $transactions = $transactions->between(Input::get('from'), Input::get('to'))->get();
        }

        return $transactions = $records->get();
    }
}