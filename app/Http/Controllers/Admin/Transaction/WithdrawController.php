<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Agenwin\GameTransactionLog;

use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;
use App\Repositories\Contracts\TransactionStatusRepositoryInterface;
use App\Repositories\Contracts\GameTransactionLogRepositoryInterface;

use App\Http\Requests;
use FatalErrorException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class WithdrawController extends Controller
{
    /**
     * @var App\Repositories\Eloquent\Transaction\DbTransactionRepository
     */
    private $transaction;

    /**
     * @var App\Repositories\Eloquent\Transaction\DbTransactionStatusRepository
     */
    private $transaction_status;

    /**
     * @var App\Repositories\Eloquent\UserGameProfile\DbUserGameProfileRepository;
     */
    private $user_game_profile;

    /**
     * @var App\Repositories\Eloquent\GameType\DbGameTypeRepository;
     */
    private $game_type;
    private $game_transaction_log;

    public function __construct(GameTypeRepositoryInterface $game_type,
                                TransactionRepositoryInterface $transaction,
                                UserGameProfileRepositoryInterface $user_game_profile,
                                TransactionStatusRepositoryInterface $transaction_status, 
                                GameTransactionLogRepositoryInterface $game_transaction_log)
    {
        $this->transaction = $transaction;
        $this->transaction_status = $transaction_status;
        $this->user_game_profile = $user_game_profile;
        $this->game_type = $game_type;
        $this->game_transaction_log = $game_transaction_log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        $status = $this->transaction_status->getStatusWhereNotIn(['5']);
        $transactions = $this->getTransaction();
        return view('admin.withdraw.index', compact('transactions', 'status'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = $this->transaction->with(['user.profile', 'bank.bankName', 'status'])->find($id);

        try {
            $gameAccount = $this->user_game_profile->findFirstWhere(['user_id' => $transaction->user_id, 'game_id' => $transaction->game_id]);
            $game_log = $this->game_transaction_log->findFirstWhere(['transaction_id' => $id]);
        }
        catch (ModelNotFoundException $e) {
            $gameAccount = [];
        }
        catch(FatalErrorException $e) {
            $game_log = 0;
        }

        $status = $this->transaction_status->getStatusWhereNotIn(['1','2','7']);

        return view('admin.withdraw.show', compact('transaction', 'status', 'gameAccount' ,'game_log'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['remarks' => 'required']);
        $request->merge(['modify_by' => auth()->user()->username, 'completed_at' => date('Y-m-d')]);

        if ($this->transaction->update($request->except('_method', '_token'), $id))
        {
            session()->flash('flash_message', 'Transaction has been updated');
        }

        return redirect()->back();
    }

    /**
     * Export data to excel.
     *
     * @return Maatwebsite\Excel\Facades\Excel
     */
    public function export()
    {
       return Excel::create('Withdraw Transactions', function( $excel ) {

            $excel->sheet('Sheet 1', function( $sheet ) {

                $sheet->setFontFamily('Comic Sans MS');
                $transactions = $this->getTransaction();
                $sheet->loadView('admin.withdraw.xls')->with('transactions', $transactions);

            });

        })->export('xls');
    }

    public function getTransaction()
    {
        $records = $this->transaction->getWithdrawTransactions();

        switch (Input::get('status'))
        {
            case 1:
                $transactions = $records;
                break;
            case 2:
                $transactions = $records->pending();
                break;
            case 3:
                $transactions = $records->approved();
                break;
            case 4:
                $transactions = $records->rejected();
                break;
            case 5:
                $transactions = $records->tobeapproved();
                break;
            case 6:
                $transactions = $records->processing();
                break;
            case 7:
                $transactions = $records->cancelled();
                break;
            default:
                $transactions = $records->pending();
                break;
        }

        if (Input::get('from') != null && Input::get('to') != null)
        {
            return $transactions = $transactions->between(Input::get('from'), Input::get('to'))->get();
        }

        return $transactions = $records->get();
    }
}