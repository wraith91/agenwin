<?php

namespace App\Http\Controllers\Admin\Member;

use App\Agenwin\User;
use App\Agenwin\Role;
use App\Agenwin\GameType;
use App\Agenwin\Transaction;
use App\Agenwin\GameTransactionLog;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\GameTransactionLogRepositoryInterface;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MemberController extends Controller
{
    /**
     * @var App\Repositories\User\UserRepositoryInterface
     */
    private $user;

    /**
     * @var App\Repositories\Contracts\GameTypeRepositoryInterface
     */
    private $game_type;

    /**
     * @var App\Repositories\Contracts\GameTransactionLogRepositoryInterface
     */
    private $game_transaction_log;

    public function __construct(UserRepositoryInterface $user, GameTypeRepositoryInterface $game_type, GameTransactionLogRepositoryInterface $game_transaction_log)
    {
        $this->user = $user;
        $this->game_type = $game_type;
        $this->game_transaction_log = $game_transaction_log;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request = null)
    {
        if ($request->get('search'))
        {
            try
            {
                $user = $this->user->getInfo($request->get('search'));
                $max_balance = $this->user->getMaxBalance($request->get('search'));
                $deposits = $user->transaction()->deposit()->orderBy('created_at', 'desc')->limit(10)->get();
                $withdraws = $user->transaction()->withdraw()->orderBy('created_at', 'desc')->limit(10)->get();
            }
            catch (ModelNotFoundException $e)
            {
                return redirect()->intended('admin/members/information')->withErrors('No information was found...');
            }

            $results = [
                'max_balance' => $max_balance->transaction->sum('amount'),
                'latest_trxn' => $user->transaction()->orderBy('created_at', 'desc')->first(['created_at'])
            ];

            if (count($user->transaction) <= 0) {
                $results = [
                    'max_balance' => 0,
                    'latest_trxn' => ['created_at' => 'No transaction created...']
                ];
            }
        }

        $game_type = $this->game_type->lists('name', 'id');
        return view('admin.member.index', compact('user', 'deposits', 'withdraws', 'game_type', 'results'));
    }

    public function show($name, $id)
    {
        $logs = $this->game_transaction_log->findWhere(['game_profile_id' => $id]);
        return view('admin.member.show', compact('logs'));
    }

    public function edit($id)
    {
        $user = $this->user->with('profile')->find($id);
        return view('admin.member.changepass.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['password' => 'required|min:8', 'confirm_password' => 'required|same:password']);

        if ($this->user->update($request->only('password'), $id))
        {
            session()->flash('flash_message', 'Member password has been successfully changed!');
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->delete($id);
        session()->flash('flash_message', $user->username . ' account has been successfully deleted!');
        return redirect()->back();
    }

    /**
     * List all members account
     *
     * @param  Request $request
     * @return mixed
     */
    public function lists(Request $request)
    {
        $users = $this->user->getAll();
        return view('admin.member.lists', compact('users'));
    }
}
