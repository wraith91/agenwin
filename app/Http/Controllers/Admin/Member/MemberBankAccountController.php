<?php

namespace App\Http\Controllers\Admin\Member;

use App\Agenwin\User;
use App\Agenwin\UserBankProfile;
use App\Repositories\Contracts\UserRepositoryInterface;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MemberBankAccountController extends Controller
{
    private $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['account_name' => 'required', 'account_no' => 'required']);

        if ($this->user->updateBankAccount($request->except('_method', '_token'), $id))
        {
            session()->flash('flash_message', 'Member bank account details has been successfully changed!');
        }

        return redirect()->back();
    }
}
