<?php

namespace App\Http\Controllers\Admin\Member;

use App\Agenwin\GameType;
use App\Agenwin\UserGameProfile;
use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;


use App\Http\Requests;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class MemberGameAccountController extends Controller
{
    /**
     * @var App\Repositories\Contracts\GameTypeRepositoryInterface
     */
    private $game_type;

    /**
     * @var App\Repositories\Contracts\UserGameProfileRepositoryInterface
     */
    private $user_game_profile;

    public function __construct(GameTypeRepositoryInterface $game_type, UserGameProfileRepositoryInterface $user_game_profile)
    {
        $this->game_type = $game_type;
        $this->user_game_profile = $user_game_profile;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.member.game_id_lists.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = $this->user_game_profile->getAllWithGameType($id);
        return view('admin.member.game_id_lists.index', compact('records', 'id'));
    }

    public function export()
    {
        return Excel::create('Game Account Lists', function( $excel ) {

            $excel->sheet('Sheet 1', function( $sheet ) {
                $records = $this->user_game_profile->getAllWithGameType(Input::get('id'));
                $sheet->loadView('admin.member.game_id_lists.csv')->with('records', $records);
            });

        })->export('csv');
    }
}
