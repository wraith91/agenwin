<?php

namespace App\Http\Controllers\Admin\Content;

use App\Repositories\Contracts\ArticleRepositoryInterface;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleFormRequest;

class ArticlesController extends Controller
{
    /**
     * @var App\Repositories\Contracts\ArticleRepositoryInterface
     */
    protected $article;

    public function __construct(ArticleRepositoryInterface $article)
    {
        $this->article = $article;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->article->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.content.article.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.content.article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleFormRequest $request)
    {
        $imageFile = $request->file('image');
        $inputs = $request->except('_token', 'image');

        if ($request->hasFile('image'))
        {
            $name = time() . '-' . $imageFile->getClientOriginalName();
            $path = $imageFile->move('/var/www/html/upload', $name);
            $inputs = $inputs + ['image' => $name];
        }

        $this->article->createArticle($inputs);
        session()->flash('flash_message', 'Blog has been created!');
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = $this->article->find($id);
        return view('admin.content.article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $imageFile = $request->file('image');
        $this->validate($request, ['title' => 'required', 'body' => 'required']);
        $inputs = $request->except('_method', '_token', 'image');

        if ($request->hasFile('image'))
        {
            $name = time() . '-' . $imageFile->getClientOriginalName();
            // $path = $imageFile->move(public_path() . '/main/app/img/article', $name);
            $path = $imageFile->move('/var/www/html/upload', $name);
            $inputs = $inputs + ['image' => $name];
        }

        $article = $this->article->update($inputs, $id);
        session()->flash('flash_message', $article->title . ' has been updated!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = $this->article->delete($id);
        session()->flash('flash_message', $article->title . ' article has been deleted!');
        return redirect()->back();
    }
}
