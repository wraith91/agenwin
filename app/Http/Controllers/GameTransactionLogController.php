<?php

namespace App\Http\Controllers;

use App\Agenwin\Transaction;
use App\Agenwin\UserGameProfile;
use App\Agenwin\GameTransactionLog;

use App\Repositories\Contracts\TransactionRepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;
use App\Repositories\Contracts\GameTransactionLogRepositoryInterface;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameTransactionLogController extends Controller
{
    /**
     * @var App\Repositories\Contracts\GameTransactionLogRepositoryInterface
     */
    private $game_trxn_log;

    /**
     * @var App\Repositories\Contracts\TransactionRepositoryInterface
     */
    private $transaction;

    /**
     * @var App\Repositories\Contracts\UserGameProfileRepositoryInterface
     */
    private $user_game_profile;

    public function __construct(GameTransactionLogRepositoryInterface $game_trxn_log,
                                TransactionRepositoryInterface $transaction,
                                UserGameProfileRepositoryInterface $user_game_profile)
    {
        $this->game_trxn_log = $game_trxn_log;
        $this->transaction = $transaction;
        $this->user_game_profile = $user_game_profile;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = $this->game_trxn_log->with('transaction.user')->orderBy('created_at', 'desc')->get();
        return view('admin.log.index', compact('logs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $transaction = Transaction::select('amount')->find($request->transaction_id);

        $total = $request->from - $request->to;

        if ($total != abs($transaction->amount))
        {
            return redirect()->back()->withErrors('Invalid difference from transaction log.');
        }

        $gameProfile = UserGameProfile::with('transactionLogs')->find($id);

        $transactionLog = new GameTransactionLog($request->except('_token'));

        if ($gameProfile->transactionLogs()->save($transactionLog))
        {
            session()->flash('flash_message', 'Withdrawal log has been created.');
        }

        return redirect()->back();
    }
}
