<?php

namespace App\Http\Controllers\App\Content;

use App\Repositories\Contracts\ArticleRepositoryInterface;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    protected $article;

    public function __construct(ArticleRepositoryInterface $article)
    {
        $this->article = $article;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->article->orderBy('created_at', 'desc')->paginate(8);
        return view('app.article.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($title)
    {
        $article = $this->article->getArticleByTitle($title);
        return view('app.article.show', compact('article'));
    }
}
