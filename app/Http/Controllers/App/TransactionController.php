<?php

namespace App\Http\Controllers\App;

use App\Agenwin\GameType;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Contracts\GameTypeRepositoryInterface;
use App\Repositories\Contracts\TransactionRepositoryInterface;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * @var App\Repositories\Eloquent\Transaction\DbTransactionRepository
     */
    protected $transaction;

    /**
     * @var App\Repositories\Eloquent\User\DbUserRepository
     */
    protected $user;

    public function __construct(TransactionRepositoryInterface $transaction, UserRepositoryInterface $user)
    {
        $this->transaction = $transaction;
        $this->user = $user;
    }

    public function index(Request $request = null)
    {
        if ($request->get('trxn_type_id'))
        {
            $history = $this->transaction->getAllByUserId();

            if ($request->get('trxn_type_id') == 1)
            {
                $transactions = $history->deposit();
            }

            if ($request->get('trxn_type_id') == 2)
            {
                $transactions = $history->withdraw();
            }

            $transactions = $history->get();
        }
        else
        {
            $transactions = null;
        }

        return view('app.transaction.history', compact('transactions'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['amount' => 'required|integer|min:50', 'trxn_type_id' => 'required|integer|max:2|min:1']);

        if ($request->trxn_type_id == 2)
        {
            $negative = 0;
            $negative -= $request->amount;
            $request->offsetSet('amount', $negative);
        }

        $inputs = $request->except('_token', 'account_name', 'account_no');

        if($this->transaction->createTransaction($inputs))
        {
            session()->flash('flash_message', 'Your transaction has been created');
        }

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = $this->transaction->find($id);

        if ($transaction->trxn_status_id > 2)
        {
            return redirect()->back()->withErrors('Your transaction is currently on process..');
        }

        if ($this->transaction->cancelTransaction($id))
        {
            session()->flash('flash_message', 'Transaction has been cancelled');
        }

        return redirect()->back();
    }

    /**
     * Display deposit page
     *
     * @return \Illuminate\Http\Response
     */
    public function deposit(GameTypeRepositoryInterface $game_type)
    {
        try {
            $user = $this->user->getDepositRecord();
            $game_type = $game_type->lists('name', 'id');
        }
        catch(ModelNotFoundException $e) {
            return redirect()->home();
        }

        if (in_array_r('Pending', $user->transaction->toArray()))
        {
            session()->flash('info_message', 'You still have pending deposit transaction.');
        }

        return view('app.transaction.deposit', compact('user', 'game_type'));
    }

    /**
     * Display withdraw page
     *
     * @return \Illuminate\Http\Response
     */
    public function withdraw()
    {
        try
        {
            $user = $this->user->getWithdrawRecord();
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }

        foreach ($user->gameAccount as $types)
        {
            $type[$types->gameType->id] = $types->gameType->name;
        }

        return view('app.transaction.withdraw', compact('user', 'type'));
    }
}
