<?php

namespace App\Http\Controllers\App;

use App\Agenwin\BankType;
use App\Repositories\Contracts\UserRepositoryInterface;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileFormRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ProfilesController extends Controller
{
    /**
     * @var App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $user;

    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $username
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileFormRequest $request, $id)
    {
        if ($this->user->updateProfile($request->except('_method', '_token'), $id))
        {
            session()->flash('flash_message', 'Your profile has been updated');
            return redirect()->back();
        }

        return redirect()->back()->withErrors('Invalid data!');
    }

    /**
     * Display the specified resource.
     *
     * @param  $username
     * @return view
     */
    public function show($username)
    {
        try
        {
            $user = $this->user->getRecordByUsername(['profile', 'bankAccount'], $username);
            $bank_name = BankType::lists('name', 'id');
        }
        catch(ModelNotFoundException $e)
        {
            return redirect()->home();
        }

        return view('app.profile.show', compact('user', 'bank_name'));
    }

    public function settings($username)
    {
        return view('app.profile.setting');
    }

    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, ['old_password' => 'required|min:8', 'password' => 'min:8|confirmed', 'password_confirmation' => 'min:8']);

        if (!Hash::check($request->old_password, auth()->user()->password))              //check what bank is submitted
        {
            return redirect()->back()->withErrors('Old password does not exist.');      //apply custom rule for each bank
        }

        if ($this->user->update($request->except('_method', '_token'), $id))
        {
            session()->flash('flash_message', 'Change password successfully!');
        }

        return redirect()->back();
    }
}
