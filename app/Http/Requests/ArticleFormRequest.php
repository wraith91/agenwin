<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $input = $this->all();
        $input['title']  = strtolower(trim($input['title']));
        $input['body'] = trim($input['body']);
        $this->replace($input);

        return [
            'title'        => 'required',
            'body'         => 'required',
            'published_at' => 'required',
            'image'        => 'required'
        ];
    }
}
