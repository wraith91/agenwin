<?php

// Event::listen('illuminate.query', function($query)
// {
//     var_dump($query);
// });
Route::group(['middleware' => 'user'], function () {
	/**
	 * Application Static Routes
	 */
	Route::get('/', ['as' => 'home', 'uses' => 'App\PagesController@home']);
	Route::get('/casino', 'App\PagesController@casino');
	Route::get('/casino-sbobet', 'App\PagesController@casinoSbobet');
	Route::get('/casino-calibet', 'App\PagesController@casinoCalibet');
	Route::get('/casino-ibcbet', 'App\PagesController@casinoIbcbet');
	Route::get('/sabung-ayam', 'App\PagesController@sabungAyam');
	// Route::get('/game-sabung', 'App\PagesController@gameSabung');
	Route::get('/poker', 'App\PagesController@poker');
	Route::get('/promo', 'App\PagesController@promo');
	Route::get('/help', 'App\PagesController@help');
	Route::get('/contact', 'App\PagesController@contact');
	Route::get('/beritabola', 'App\PagesController@beritabola');
	Route::get('/livescore', 'App\PagesController@livescore');
	Route::get('/tebakbola', 'App\PagesController@tebakbola');
	Route::post('/contact', 'InquiryController@store');
	Route::get('/auth/login', 'App\PagesController@login');

	Route::get('/news', 'App\Content\ArticleController@index');
	Route::get('/news/{title}', 'App\Content\ArticleController@show');

});

/**
 * Registration
 */

Route::get('register', ['as' => 'register', 'uses' => 'App\RegistrationController@index', 'middleware' => 'guest']);
Route::post('register', ['as' =>'registration.store', 'uses' => 'App\RegistrationController@store']);

/**
 * Authentication
 */

Route::get('auth/login', ['as' => 'auth/login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'auth.postLogin', 'uses' =>  'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' =>  'Auth\AuthController@getLogout', 'middleware' => 'auth']);

Route::resource('sessions', 'Admin\Login\SessionsController', ['only' => ['create', 'store']]);
Route::resource('admin/auth/login', 'Admin\Login\AdminController', ['only' => ['create', 'store']]);
Route::get('login', ['as' => 'login', 'uses' => 'Admin\Login\SessionsController@create']);
Route::get('admin/auth/login', ['as' => 'admin.auth.login', 'uses' => 'Admin\Login\AdminController@create', 'middleware' => 'restricted']);



/**
 * -------------------------------------------
 * Member function
 * -------------------------------------------
 */

Route::group(['as' => 'member::', 'middleware' => 'auth'], function () {

	/**
	 * User Transaction
	 */
	Route::get('game/account/{id}', 'UserGameProfileController@show');
	Route::post('transaction', ['as' => 'transaction:create', 'uses' => 'App\TransactionController@store']);
	Route::get('transaction/history', ['as' => 'transaction:history', 'uses' => 'App\TransactionController@index']);
	Route::get('transaction/deposit', ['as' => 'transaction:deposit', 'uses' => 'App\TransactionController@deposit']);
	Route::get('transaction/withdraw', ['as' => 'transaction:withdraw', 'uses' => 'App\TransactionController@withdraw']);
	Route::patch('transaction/withdraw/{id}', ['as' => 'transaction/withdraw/{id}', 'uses' => 'App\TransactionController@update']);

	/**
	 * User Profile
	 */
	Route::resource('profile', 'App\ProfilesController', ['only' => ['update', 'show']]);
	Route::get('/{profile}', ['as' => 'profile', 'uses' => 'App\ProfilesController@show']);
	Route::put('/{profile}', ['as' => 'profile.update', 'uses' => 'App\ProfilesController@update']);
	Route::get('/{profile}/settings', 'App\ProfilesController@settings');
	Route::patch('/settings/{id}', 'App\ProfilesController@updatePassword');

});

/**
 * -------------------------------------------
 * Administrator function
 * -------------------------------------------
 */

Route::group(['as' => 'admin::', 'middleware' => ['auth', 'authorized']], function () {

  	/**
	 * Dashboard
	 */
  	Route::get('admin/dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

  	/**
	 * Member
	 */
	Route::get('admin/members/lists', 'Admin\Member\MemberController@lists');
	Route::patch('admin/members/{id}', 'Admin\Member\MemberController@update');
	Route::get('admin/members/{id}/edit', 'Admin\Member\MemberController@edit');
	Route::delete('admin/members/{id}', ['as' => 'member:delete', 'uses' => 'Admin\Member\MemberController@destroy']);
	Route::get('admin/members/information', ['as' => 'members/information', 'uses' => 'Admin\Member\MemberController@index']);

	/**
	 * Member - BankAccount
	 */
	Route::patch('admin/members/bankaccount/{id}', 'Admin\Member\MemberBankAccountController@update');


	Route::post('admin/transactions/gameprofile/{id}', 'UserGameProfileController@store');
	Route::get('admin/members/information/account/{name}/{id}', 'Admin\Member\MemberController@show');

	Route::get('admin/members/game-id-lists/export', 'Admin\Member\MemberGameAccountController@export');
	Route::resource('admin/members/game-id-lists', 'Admin\Member\MemberGameAccountController', ['only' => ['index', 'show']]);

 	/**
	 * Transaction
	 */
	Route::get('admin/transactions/deposit/export', 'Admin\Transaction\DepositController@export');
	Route::get('admin/transactions/withdraw/export', 'Admin\Transaction\WithdrawController@export');
  	Route::resource('admin/transactions/deposit', 'Admin\Transaction\DepositController', ['only' => ['index', 'show', 'update']]);
	Route::resource('admin/transactions/withdraw', 'Admin\Transaction\WithdrawController', ['only' => ['index', 'show', 'update']]);
	Route::get('admin/transactions/log', 'GameTransactionLogController@index');
	Route::post('admin/transactions/log/{id}', 'GameTransactionLogController@store');


	Route::get('admin/bonus/history/{id}', 'Admin\Bonus\BonusController@index');
	Route::get('admin/bonus/game/{id}', 'Admin\Bonus\BonusController@show');
	Route::patch('admin/bonus/game/{id}', 'Admin\Bonus\BonusController@update');
	Route::post('admin/bonus/upload', 'Admin\Bonus\BonusController@upload');

  	/**
	 * Inquiry
	 */
	Route::resource('admin/inquiry', 'InquiryController');

  	/**
	 * Article
	 */
	Route::resource('admin/article', 'Admin\Content\ArticlesController', ['except' => ['show']]);

	/**
	 * Account
	 */
	Route::get('admin/account/settings', ['as' => 'admin.account.settings.create' ,'uses' => 'Admin\AccountSettingsController@create']);
	Route::put('admin/account/settings/{id}', ['as' => 'admin.account.settings.update' ,'uses' => 'Admin\AccountSettingsController@update']);

	Route::group(['middleware' => 'admin'], function () {
		Route::resource('admin/system/user', 'Admin\AuthorizeUserController');
	});

});