<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class UserBankProfile extends Model
{
	/**
   * The database table used by the model.
   *
   * @var string
   */
	protected $table = 'user_bank_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bank_type_id', 'account_name', 'account_no'];


    /**
     * BankProfile belongs to user.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
		return $this->belongsTo('App\Agenwin\User', 'user_id');
    }

    /**
     * Each BankProfile has one bank type.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankName()
    {
		return $this->hasOne('App\Agenwin\BankType', 'id', 'bank_type_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'id');
    }
}
