<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class BonusStatus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bonus_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
    public $timestamps = false;

    public function bonusPoint()
    {
        return $this->belongsTo(Bonus::class, 'id', 'bonus_status_id');
    }
}
