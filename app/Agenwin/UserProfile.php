<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
		protected $table = 'user_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'birth_date', 'mobile', 'gender'];

    /**
     * The dates attributes that are converted to carbon instance
     *
     * @var array
     */
    protected $dates = ['birth_date'];

    /**
     * Always set first name to lowercase when we save it to the db
     *
     * @param string
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst(strtolower($value));
    }

    /**
     * Always set last name to lowercase when we save it to the db
     *
     * @param string
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucfirst(strtolower($value));
    }

	/**
     * Each Profile belongs to one user.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
    		return $this->belongsTo(User::class, 'id');
    }
}
