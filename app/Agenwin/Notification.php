<?php

namespace App\Agenwin;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
		private $relatedObject = null;
		/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'type', 'subject', 'body', 'is_read', 'sent_at'];

    protected $dates = ['sent_at'];

    public function getDates()
    {
    		return ['created_at', 'updated_at', 'sent_at'];
    }

    public function withSubject($subject)
		{
		    $this->subject = $subject;
		 
		    return $this;
		}
		 
		public function withBody($body)
		{
		    $this->body = $body;
		 
		    return $this;
		}
		 
		public function withType($type)
		{
		    $this->type = $type;
		 
		    return $this;
		}
		 
		public function regarding($object)
		{
		    if(is_object($object))
		    {
		        $this->object_id   = $object->id;
		        $this->object_type = get_class($object);
		    }
		 
		    return $this;
		}

		public function deliver()
		{
		    $this->sent_at = new Carbon;
		    $this->save();
		 
		    return $this;
		}	

		/**
		 * Transaction is created by the user
		 * 
		 * @return Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
    public function user()
    {
    		return $this->belongsTo('App\User');
    }
}
