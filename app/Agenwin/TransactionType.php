<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class TransactionType extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
    protected $table = 'transaction_types';

		/**
	   * Fillable fields for a TransactionType
	   *
	   * @var array
	   */
    protected $fillable = ['name'];

		/**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
		public $timestamps = false;
}
