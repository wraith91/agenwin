<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class InquiryStatus extends Model
{
		/**
	   * The database table used by the model.
	   *
	   * @var string
	   */
    protected $table = 'inquiry_status';

		/**
	   * Fillable fields for a InquiryType
	   *
	   * @var array
	   */
		protected $fillable = ['name'];

		/**
	   * The timestamp is disabled in this model.
	   *
	   * @var string
	   */
		public $timestamps = false;

    public function status()
    {
        return $this->belongsTo(Inquiry::class, 'id', 'inquiry_status_id');
    }
}
