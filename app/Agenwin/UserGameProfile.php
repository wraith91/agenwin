<?php

namespace App\Agenwin;

use Illuminate\Database\Eloquent\Model;

class UserGameProfile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_game_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'game_id', 'username', 'password'];

    /**
     * User has many game accounts
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * User has one game type
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function gameType()
    {
        return $this->hasOne(GameType::class, 'id', 'game_id');
    }

    /**
     * Game Account has many transaction logs
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactionLogs()
    {
        return $this->hasMany(GameTransactionLog::class, 'game_profile_id');
    }

    /**
     * Filter the gametype of the user game account
     *
     * @param  [type] $query
     * @param  int $search
     * @return query
     */
    public function scopeType($query, $search)
    {
        return $query->where('game_id', $search);
    }
}
