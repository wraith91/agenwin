<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use App\Repositories\RepositoryException;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class BaseRepository implements RepositoryInterface {

    /**
     * @var App
     */
	private $app;

    /**
     * @var
     */
	protected $model;

    /**
     * @param App   $app
     * @throws \App\Repositories\Exceptions\RepositoryException
     */
	public function __construct(App $app)
	{
		$this->app = $app;
		$this->makeModel();
	}

    /**
     * Specify Model class name
     *
     * @return mixed
     */
	abstract public function model();

/**
     * Retrieve all data of repository
     *
     * @param array $columns
     *
     * @return mixed
     */
    public function all($columns = ['*'])
    {
    	return $this->model->get($columns);
    }

    /**
     * Retrieve all data of repository, paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'])
    {
    	return $this->model->paginate($limit, $columns);
    }

    /**
     * Retrieve all data of repository, simple paginated
     *
     * @param null  $limit
     * @param array $columns
     *
     * @return mixed
     */
    public function simplePaginate($limit = null, $columns = ['*'])
    {
    	return $this->model->paginate($limit, $columns);
    }

    /**
     * Find data by id
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
    	return $this->model->findOrFail($id, $columns);
    }

    /**
     * Find first data or fail
     *
     * @param       $id
     * @param array $columns
     *
     * @return mixed
     */
    public function firstOrFail()
    {
    	return $this->model->firstOrFail();
    }

    /**
     * Find data by field and value
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     *
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*'])
    {
    	return $this->model->where($field, '=', $value)->get($columns);
    }

    /**
     * Find data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhere(array $where, $columns = ['*'])
    {
    	$this->applyConditions($where);

    	return $this->model->get($columns);
    }

    /**
     * Find the first data by multiple fields
     *
     * @param array $where
     * @param array $columns
     *
     * @return mixed
     */
    public function findFirstWhere(array $where, $columns = ['*'])
    {
        $this->applyConditions($where);

        return $this->model->first($columns);
    }

    /**
     * Find data by multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereIn($field, array $values, $columns = ['*'])
    {
    	return $this->model->whereIn($field, $value)->get($columns);
    }

    /**
     * Find data by excluding multiple values in one field
     *
     * @param       $field
     * @param array $values
     * @param array $columns
     *
     * @return mixed
     */
    public function findWhereNotIn($field, array $values, $columns = ['*'])
    {
    	return $this->model->whereNotIn($field, $values)->get($columns);
    }

    /**
     * Save a new entity in repository
     *
     * @param array $attributes
     *
     * @return mixed
     */
    public function create(array $attributes)
    {
    	$model = $this->model->newInstance($attributes);
        $model->save();
        return $model;
    }

    /**
     * Update a entity in repository by id
     *
     * @param array $attributes
     * @param       $id
     *
     * @return mixed
     */
    public function update(array $attributes, $id)
    {
    	$model = $this->model->findOrFail($id);
    	$model->fill($attributes)->save();
    	return $model;
    }

    /**
     * Update or Create an entity in repository
     *
     * @throws ValidatorException
     *
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    public function updateOrCreate(array $attributes, array $values = [])
    {
    	return $this->model->updateOrCreate($attributes, $values);
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
    	$model = $this->model->findOrFail($id);
    	$model->delete();
        return $model;
    }

    /**
     * Order collection by a given column
     *
     * @param string $column
     * @param string $direction
     *
     * @return $this
     */
    public function orderBy($column, $direction = 'asc')
    {
        $this->model = $this->model->orderBy($column, $direction);
        return $this;
    }

    /**
     * Retrieving a list of column values
     *
     * @param string $column
     *
     * @return mixed
     */
    public function lists($column)
    {
        return $this->model->lists($column, 'id');
    }

    /**
     * Applies the given where conditions to the model.
     *
     * @param array $where
     * @return void
     */
    protected function applyConditions(array $where)
    {
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $this->model = $this->model->where($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }
    }

    /**
     * Load relations
     *
     * @param array|string $relations
     *
     * @return $this
     */
    public function with($relations)
    {
        $this->model = $this->model->with($relations);
        return $this;
    }

    /**
     * Check if entity has relation
     *
     * @param string $relation
     *
     * @return $this
     */
    public function has($relation)
    {
        $this->model = $this->model->has($relation);
        return $this;
    }

    /**
     * Load relation with closure
     *
     * @param string $relation
     * @param closure $closure
     *
     * @return $this
     */
    public function whereHas($relation, $closure)
    {
        $this->model = $this->model->whereHas($relation, $closure);
        return $this;
    }

    /**
     * Find data using username
     *
     * @param string $relation
     * @param closure $closure
     *
     * @return $this
     */
    public function whereUsername($username)
    {
    	$this->model = $this->model->whereUsername($username);
    	return $this;
    }

    public function get()
    {
    	return $this->model->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws RepositoryException
     */
	public function makeModel()
	{
		$model = $this->app->make($this->model());

		if(!$model instanceof Model)
			throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

		return $this->model = $model;
	}
}