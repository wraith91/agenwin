<?php

namespace App\Repositories;
/**
 * Class RepositoryException
 * @package App\Repositories
 */
class RepositoryException extends \Exception {}