<?php

namespace App\Repositories\Contracts;

interface UserRepositoryInterface
{
	/**
	 * Displaying list of all members with BankAccount details
	 *
	 * @return mixed
	 */
	function getAll();

	/**
	 * Finding record of a member with Profile, GameAccount, GameType and Transaction details
	 *
	 * @param  string $username
	 * @return mixed
	 */
	public function getInfo($username);

	/**
	 * Get the max balance of a member
	 *
	 * @param  string $username
	 * @return mixed
	 */
	public function getMaxBalance($username);

	/**
	 * Get record by username
	 *
	 * @param  array $relations
	 * @param  string $username
	 * @return mixed
	 */
	public function getRecordByUsername(array $relations, $username);

	/**
	 * Update member profile
	 *
	 * @param  array 	$attributes
	 * @param  string 	$username
	 * @return mixed
	 */
	public function updateProfile($attributes, $id);

	/**
	 * Update member profile by id
	 *
	 * @param  array 	$attributes
	 * @param  string 	$username
	 * @return mixed
	 */
	public function updateProfileById($attributes, $id);

	/**
	 * Update member's bankAccount
	 *
	 * @param  array 	$attributes
	 * @param  int 		$id
	 * @return mixed
	 */
	public function updateBankAccount($attributes, $id);

	/**
	 * Get the withdraw record of a member
	 *
	 * @return mixed
	 */
	public function getWithdrawRecord();

	/**
	 * Get the deposit record of a member
	 *
	 * @return mixed
	 */
	public function getDepositRecord();

	/**
	 * Create an authorize user account
	 *
	 * @param  array $attributes
	 * @return mixed
	 */
	public function createAuthorizeUser(array $attributes);
}