<?php

namespace App\Repositories\Contracts;

interface ArticleRepositoryInterface
{
	/**
	 * User Create an Article
	 *
	 * @param  array $attributes
	 * @return array
	 */
	public function createArticle(array $attributes);

	/**
	 * Get an article by title
	 *
	 * @param  string $title
	 * @return mixed
	 */
	public function getArticleByTitle($title);
}