<?php

namespace App\Repositories\Contracts;

interface UserGameProfileRepositoryInterface
{
	/**
	 * Get all UserGameProfile with GameType
	 *
	 * @param  int $search
	 * @return mixed
	 */
	public function getAllWithGameType($search);
}