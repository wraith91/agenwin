<?php

namespace App\Repositories\Contracts;

interface TransactionRepositoryInterface
{
    /**
     * Get all deposit transaction
     *
     * @return collection
     */
    public function getDepositTransactions();

    /**
     * Get all withdraw transaction
     *
     * @return collection
     */
    public function getWithdrawTransactions();

    /**
     * Get all transaction records by user id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAllByUserId();

    /**
     * User create a transaction (Deposit or Withdraw)
     *
     * @param  array $attributes
     * @return mixed
     */
    public function createTransaction(array $attributes);

    /**
     * User cancel a transaction
     *
     * @param  int $id
     * @return bolean
     */
    public function cancelTransaction($id);
}