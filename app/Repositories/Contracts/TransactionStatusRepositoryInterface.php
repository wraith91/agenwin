<?php

namespace App\Repositories\Contracts;

interface TransactionStatusRepositoryInterface
{
    /**
     * Get all the status where not in.
     *
     * @return \Illuminate\Http\Response
     */
	public function getStatusWhereNotIn(array $id);

    /**
     * Get all the status where in.
     *
     * @return \Illuminate\Http\Response
     */
	public function getStatusWhereIn(array $id);
}