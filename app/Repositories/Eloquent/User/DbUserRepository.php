<?php

namespace App\Repositories\Eloquent\User;

use App\Agenwin\User;
use App\Agenwin\UserProfile;
use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

class DbUserRepository extends BaseRepository implements UserRepositoryInterface
{
	function model()
	{
		return User::class;
	}

	/**
	 * Displaying list of all members with BankAccount details
	 *
	 * @return mixed
	 */
	function getAll()
	{
		return $this->model->with(['bankAccount', 'bankAccount.bankName'])
						  ->whereHas('roles', function ($query) {
                          	$query->where('name', 'member');
                          })->get();
	}

	/**
	 * Finding record of a member with Profile, GameAccount, GameType and Transaction details
	 *
	 * @param  string $username
	 * @return mixed
	 */
	public function getInfo($username)
	{
		return $this->model->with('profile', 'gameAccount.gameType', 'transaction')
						  ->whereHas('roles', function ($query) {
                      		$query->where('name', 'member');
                          })->search(trim($username))->firstOrFail();
	}

	/**
	 * Get the max balance of a member
	 *
	 * @param  string $username
	 * @return mixed
	 */
	public function getMaxBalance($username)
	{
		return $this->model->with(['transaction' => function ($query) {
								return $query->where('trxn_status_id', '3')->where('trxn_type_id', '1')->get();
							}])
						  ->whereHas('roles', function ($query) {
                      		$query->where('name', 'member');
                          })->search(trim($username))->firstOrFail();
	}

	/**
	 * Get record by username
	 *
	 * @param  array $relations
	 * @param  string $username
	 * @return mixed
	 */
	public function getRecordByUsername(array $relations, $username)
	{
		return $this->model->with($relations)->whereUsername($username)->firstOrFail();
	}

	/**
	 * Update member profile
	 *
	 * @param  array 	$attributes
	 * @param  string 	$username
	 * @return mixed
	 */
	public function updateProfile($attributes, $id)
	{
    	$model = $this->model->find($id);
    	return $model->profile->fill($attributes)->save();
	}

	/**
	 * Update member profile by id
	 *
	 * @param  array 	$attributes
	 * @param  string 	$username
	 * @return mixed
	 */
	public function updateProfileById($attributes, $id)
	{
    	$model = $this->model->find($id);
    	return $model->profile->fill($attributes)->save();
	}

	/**
	 * Update member's bankAccount
	 *
	 * @param  array 	$attributes
	 * @param  int 		$id
	 * @return mixed
	 */
	public function updateBankAccount($attributes, $id)
	{
    	$model = $this->model->find($id);
    	return $model->bankAccount->fill($attributes)->save();
	}

	/**
	 * Get the withdraw record of a member
	 *
	 * @return mixed
	 */
	public function getWithdrawRecord()
	{
		return $this->model->with(['transaction.status', 'gameAccount.gameType', 'transaction' => function($query) {
                	$query->where('trxn_type_id', '2')->whereNotIn('trxn_status_id', ['1', '3'])->orderBy('id', 'desc')->take(6);
            	}])->find(auth()->user()->id);
	}

	/**
	 * Get the deposit record of a member
	 *
	 * @return mixed
	 */
	public function getDepositRecord()
	{
		return $this->model->with(['bankAccount', 'transaction.gameType', 'transaction.status' ,'transaction' => function($query) {
                    $query->where('trxn_type_id', '1')->whereIn('trxn_status_id', ['2', '4'])->orderBy('id', 'desc')->take(6);
                }])->find(auth()->user()->id);
	}

	/**
	 * Create an authorize user account
	 *
	 * @param  array $attributes
	 * @return mixed
	 */
	public function createAuthorizeUser(array $attributes)
	{
		$user = $this->model->create(
			[
				'username' => $attributes['username'],
				'password' => $attributes['password'],
				'email' => $attributes['email']
			]);
        $profile = new UserProfile(
        	[
        		'first_name' => $attributes['first_name'],
        		'last_name' => $attributes['last_name'],
        		'gender' => $attributes['gender'],
        		'mobile' => $attributes['mobile']
        	]);

        $profile = $user->profile()->save($profile);
        return $user;
	}
}