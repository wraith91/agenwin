<?php

namespace App\Repositories\Eloquent\Transaction;

trait TransactionScope
{
    /**
     * Scope a query to only include deposit transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeposit($query)
    {
        return $query->where('trxn_status_id', 4);
    }

    /**
     * Scope a query to only include withdraw transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithdraw($query)
    {
        return $query->where('trxn_type_id', '2');
    }

    /**
     * Scope a query to find where
     *
     * @param         $query
     * @param  string $filterDate
     * @return query
     */
    public function scopeFrom($query, $filterDate)
    {
        return $query->where('created_at', '>=', $filterDate);
    }

    /**
     * Scope a query to find where
     *
     * @param         $query
     * @param  string $filterDate
     * @return query
     */
    public function scopeBetween($query, $from, $to)
    {
        return $query->whereBetween('created_at', [$from, $to]);
    }

    /**
     * Scope a query to only include pending transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('trxn_status_id', 2);
    }

    /**
     * Scope a query to only include approved transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApproved($query)
    {
        return $query->where('trxn_status_id', 3);
    }

    /**
     * Scope a query to only include rejected transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRejected($query)
    {
        return $query->where('trxn_status_id', 4);
    }

    /**
     * Scope a query to only include to be approved transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTobeapproved($query)
    {
        return $query->where('trxn_status_id', 5);
    }

    /**
     * Scope a query to only include processing transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProcessing($query)
    {
        return $query->where('trxn_status_id', 6);
    }

    /**
     * Scope a query to only include cancelled transactions.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCancelled($query)
    {
        return $query->where('trxn_status_id', 7);
    }
}