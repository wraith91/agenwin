<?php

namespace App\Repositories\Eloquent\Transaction;

use App\Agenwin\User;
use App\Agenwin\Transaction;
use App\Agenwin\TransactionStatus;

use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Eloquent\Transaction\TransactionScope;
use App\Repositories\Contracts\TransactionRepositoryInterface;

class DbTransactionRepository extends BaseRepository implements TransactionRepositoryInterface
{
    use TransactionScope;

	function model()
	{
		return \App\Agenwin\Transaction::class;
	}

    /**
     * Get all deposit transaction
     *
     * @return collection
     */
    public function getDepositTransactions()
    {
        return Transaction::with('status', 'user', 'bank.bankName', 'gameType')->orderBy('id', 'desc')->deposit();
    }

    /**
     * Get all withdraw transaction
     *
     * @return collection
     */
    public function getWithdrawTransactions()
    {
        return Transaction::with('status', 'user', 'bank.bankName', 'gameType')->orderBy('id', 'desc')->withdraw();
    }

    /**
     * Get all transaction records by user id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAllByUserId()
    {
        return Transaction::where('user_id', auth()->user()->id);
    }

    /**
     * User create a transaction (Deposit or Withdraw)
     *
     * @param  array $attributes
     * @return mixed
     */
    public function createTransaction(array $attributes)
    {
        $user = auth()->user();

        if (isset($attributes["trxn_user_id"]))
        {
            $user = User::find($attributes["trxn_user_id"]);
        }

        return $user->transaction()->save(new Transaction($attributes + ['bank_id' => $user->bankAccount->id]));
    }

    /**
     * User cancel a transaction
     *
     * @param  int $id
     * @return bolean
     */
    public function cancelTransaction($id)
    {
        $transaction = $this->model->find($id);
        $transaction_status = TransactionStatus::where('name', 'Cancelled')->first();
        return $transaction->fill([
                'trxn_status_id' => $transaction_status->id,
                'remarks' => 'cancelled by the user',
                'modify_by' => auth()->user()->username
            ])->save();
    }
}