<?php

namespace App\Repositories\Eloquent\TransactionStatus;

use App\Agenwin\TransactionStatus;

use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\TransactionStatusRepositoryInterface;

class DbTransactionStatusRepository extends BaseRepository implements TransactionStatusRepositoryInterface
{
	function model()
	{
		return \App\Agenwin\TransactionStatus::class;
	}

    /**
     * Get all the status where not in.
     *
     * @return \Illuminate\Http\Response
     */
	public function getStatusWhereNotIn(array $id)
	{
		return $this->model->orderBy('id', 'asc')->whereNotIn('id', $id)->lists('name', 'id')->toArray();
	}

    /**
     * Get all the status where in.
     *
     * @return \Illuminate\Http\Response
     */
	public function getStatusWhereIn(array $id)
	{
		return $this->model->orderBy('id', 'asc')->whereIn('id', $id)->lists('name', 'id')->toArray();
	}
}