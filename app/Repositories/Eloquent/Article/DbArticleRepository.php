<?php

namespace App\Repositories\Eloquent\Article;

use App\Agenwin\Article;
use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\ArticleRepositoryInterface;

class DbArticleRepository extends BaseRepository implements ArticleRepositoryInterface
{
	function model()
	{
		return Article::class;
	}

	/**
	 * User Create an Article
	 *
	 * @param  array $attributes
	 * @return array
	 */
	public function createArticle(array $attributes)
	{
		$user = auth()->user();
        $model = $this->model->newInstance($attributes);
        $user->article()->save($model);
        return $model;
	}

	/**
	 * Get an article by title
	 *
	 * @param  string $title
	 * @return mixed
	 */
	public function getArticleByTitle($title)
	{
		return $this->model->where('title', 'LIKE', "%$title%")->first();
	}
}