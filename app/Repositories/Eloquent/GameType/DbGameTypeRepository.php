<?php

namespace App\Repositories\Eloquent\GameType;

use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\GameTypeRepositoryInterface;

class DbGameTypeRepository extends BaseRepository implements GameTypeRepositoryInterface
{
	function model()
	{
		return \App\Agenwin\GameType::class;
	}
}