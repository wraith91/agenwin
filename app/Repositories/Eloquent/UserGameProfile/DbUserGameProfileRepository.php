<?php

namespace App\Repositories\Eloquent\UserGameProfile;


use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\UserGameProfileRepositoryInterface;

class DbUserGameProfileRepository extends BaseRepository implements UserGameProfileRepositoryInterface
{
	function model()
	{
		return \App\Agenwin\UserGameProfile::class;
	}

	/**
	 * Get all UserGameProfile with GameType
	 *
	 * @param  int $search
	 * @return mixed
	 */
	public function getAllWithGameType($search)
	{
		return $this->model->with('gameType')->type($search)->get();
	}
}