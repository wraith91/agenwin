<?php

namespace App\Repositories\Eloquent\GameTransactionLog;

use App\Agenwin\TransactionStatus;

use App\Repositories\BaseRepository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Contracts\GameTransactionLogRepositoryInterface;

class DbGameTransactionLogRepository extends BaseRepository implements GameTransactionLogRepositoryInterface
{
	function model()
	{
		return \App\Agenwin\GameTransactionLog::class;
	}
}