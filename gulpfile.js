var elixir = require('laravel-elixir');
require('laravel-elixir-sass-compass');
require('laravel-elixir-imagemin');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.images = {
    folder: 'components/agenwin-redesign/images',
    outputFolder: 'main/images'
};

elixir(function(mix) {
    mix.compass('admin.scss', 'public/main/css/', {
      sass: "resources/assets/sass",
      sourcemap: false
    })
    .compass(['main_desktop.scss','main_mobile.scss'], 'public/main/css/', {
      sass: "resources/assets/components/agenwin-redesign/styles",
      sourcemap: false
    })

    /*
     * User
     * Copying of Fonts
     * Concatenation of CSS
     * Concatenation of JS Files
     */

   .copy(
        'resources/assets/components/agenwin-redesign/fonts',
        'public/main/fonts'
    )
    .styles([
        'slick.css'
    ], 'public/main/css/slick.css', 'resources/assets/components/agenwin-redesign/styles')
    .scripts([
        'jquery.1.12.0.min.js',
        'jquery.waypoints.min.js',
        'jquery.js',
        'slick.min.js',
    ], 'public/main/js', 'resources/assets/components/agenwin-redesign/script')

  	/*
     * Admin
     * Copying of Img
     * Concatenation of JS Files
     */

    .copy(
        'resources/assets/components/site-newdesign/img/logo/logo-agenwin.png',
        'public/main/images'
    )
    .copy(
        'resources/assets/images/*.svg',
        'public/main/images'
    )
    .scripts([
        'components/jquery/dist/jquery.js',
        'js/iconic.min.js',
    ], 'public/main/js/vendor/admin', 'resources/assets')

    .version([
      'public/main/css/main_desktop.css',
      'public/main/css/main_mobile.css',
      'public/main/js/all.js'
    ])
    .imagemin({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    });
});
